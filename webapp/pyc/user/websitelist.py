import dbx;
import webx;
import stdx;
import json;

def main(app):
	res = {'code': stdx.XG_FAIL};
	user = app.getSession('USER');
	dbid = app.getSession('DBID');
	if user == None: res['code'] = stdx.XG_TIMEOUT;
	else:
		hdr = user + '.WEBPAGE';
		res['code'] = stdx.XG_OK;
		try:
			with app.dbconnect(dbid) as db:
				data = db.queryArray('SELECT ID,NAME AS TITLE,PARAM AS URL,FILTER AS FOLDER,REMARK AS ICON FROM T_XG_PARAM WHERE ID LIKE ? ORDER BY FOLDER ASC,ID ASC', hdr + '%');
				for item in data:
					item['id'] = item['id'][len(hdr):];
					if item['icon'].find(':') > 0: item['icon'] = '';
				res['code'] = len(data);
				res['list'] = data;
		except BaseException as e:
			app.trace('ERR', 'system error[%s]' % str(e));
			res['code'] = stdx.XG_SYSERR;
	app.setHeader('Content-Encoding', 'gzip');
	return webx.gzipencode(stdx.json(res).encode());