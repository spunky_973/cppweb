import dbx;
import webx;
import stdx;
import json;
import json;


def main(app):
	app.checkLogin();
	res = {'code': stdx.XG_FAIL};
	user = app.getParameter('user');
	passwd = app.getParameter('passwd');
	if not stdx.CheckLength(user, 64): res['code'] = stdx.XG_PARAMERR;
	elif not stdx.CheckLength(passwd, 64): res['code'] = stdx.XG_PARAMERR;
	else:
		with app.dbconnect() as db:
			arr = db.query('SELECT USER FROM T_XG_USER WHERE USER LIKE ? AND PASSWD LIKE ?', (user, passwd));
			if len(arr) > 0: res['code'] = stdx.XG_OK;
			try:
				arr = db.query('SELECT USER FROM T_XG_USER WHERE USER=? AND PASSWD=?', (user, passwd));
				if len(arr) > 0: res['code'] = stdx.XG_OK;
			except BaseException as e:
				res['code'] = stdx.XG_SYSERR;
				res['msg'] = str(e);
	return stdx.json(res);