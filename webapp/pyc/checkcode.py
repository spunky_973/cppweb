import stdx;
import imgx;
import webx;

def main(app):
	type = app.getParameter('type');
	if type == 'L' or type == 'R' or type == 'P' or type == 'DOC':
		img = imgx.CheckCode();
		code = img.getText().lower();
		if app.setSession(type + 'CHECKCODE', code) >= 0:
			app.setContentType(webx.GetMimeType(img.getFormat()));
			return img.draw();
	return stdx.XG_ERROR;