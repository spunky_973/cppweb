#ifndef XG_ENTITY_CT_XG_DBETC_H
#define XG_ENTITY_CT_XG_DBETC_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_DBETC : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	id;
	DBString	type;
	DBString	host;
	DBInteger	port;
	DBString	name;
	DBString	user;
	DBString	passwd;
	DBString	charset;
	DBInteger	enabled;
	DBInteger	step;
	DBInteger	stress;
	DBString	remark;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_DBETC";
	}

	static const char* GetColumnString()
	{
		return "ID,TYPE,HOST,PORT,NAME,USER,PASSWD,CHARSET,ENABLED,STEP,STRESS,REMARK,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
