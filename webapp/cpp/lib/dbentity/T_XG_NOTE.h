#ifndef XG_ENTITY_CT_XG_NOTE_H
#define XG_ENTITY_CT_XG_NOTE_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_NOTE : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	id;
	DBInteger	type;
	DBString	user;
	DBString	folder;
	DBString	title;
	DBBlob		icon;
	DBBlob		content;
	DBInteger	level;
	DBString	remark;
	DBInteger	position;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_NOTE";
	}

	static const char* GetColumnString()
	{
		return "ID,TYPE,USER,FOLDER,TITLE,ICON,CONTENT,LEVEL,REMARK,POSITION,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
