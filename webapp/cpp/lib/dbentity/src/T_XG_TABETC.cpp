#include "T_XG_TABETC.h"


void CT_XG_TABETC::clear()
{
	this->id.clear();
	this->dbid.clear();
	this->name.clear();
	this->title.clear();
	this->remark.clear();
	this->enabled.clear();
	this->statetime.clear();
}
int CT_XG_TABETC::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_TABETC(" + string(GetColumnString()) + ") VALUES(";
	sql += this->id.toValueString(conn->getSystemName());
	vec.push_back(&this->id);
	sql += ",";
	sql += this->dbid.toValueString(conn->getSystemName());
	vec.push_back(&this->dbid);
	sql += ",";
	sql += this->name.toValueString(conn->getSystemName());
	vec.push_back(&this->name);
	sql += ",";
	sql += this->title.toValueString(conn->getSystemName());
	vec.push_back(&this->title);
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->enabled.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_TABETC::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->id = row->getString(0);
	this->id.setNullFlag(row->isNull());
	this->dbid = row->getString(1);
	this->dbid.setNullFlag(row->isNull());
	this->name = row->getString(2);
	this->name.setNullFlag(row->isNull());
	this->title = row->getString(3);
	this->title.setNullFlag(row->isNull());
	this->remark = row->getString(4);
	this->remark.setNullFlag(row->isNull());
	this->enabled = row->getLong(5);
	this->enabled.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(6);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_TABETC::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_TABETC";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_TABETC::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return find(getPKCondition(), vec);
}
string CT_XG_TABETC::getPKCondition()
{
	string condition;
	condition = "ID=";
	condition += this->id.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_TABETC::update(bool updatenull)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_TABETC SET ";
	if (updatenull || !this->dbid.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "DBID=";
		sql += this->dbid.toValueString(conn->getSystemName());
		v.push_back(&this->dbid);
	}
	if (updatenull || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (updatenull || !this->title.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TITLE=";
		sql += this->title.toValueString(conn->getSystemName());
		v.push_back(&this->title);
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->id);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_TABETC::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_TABETC";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_TABETC::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return remove(getPKCondition(), vec);
}
string CT_XG_TABETC::getValue(const string& key)
{
	if (key == "ID") return this->id.toString();
	if (key == "DBID") return this->dbid.toString();
	if (key == "NAME") return this->name.toString();
	if (key == "TITLE") return this->title.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "ENABLED") return this->enabled.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_TABETC::setValue(const string& key, const string& val)
{
	if (key == "ID")
	{
		this->id = val;
		return true;
	}
	if (key == "DBID")
	{
		this->dbid = val;
		return true;
	}
	if (key == "NAME")
	{
		this->name = val;
		return true;
	}
	if (key == "TITLE")
	{
		this->title = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "ENABLED")
	{
		this->enabled = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_TABETC::update(bool updatenull, const string& condition, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_TABETC SET ";
	if (updatenull || !this->dbid.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "DBID=";
		sql += this->dbid.toValueString(conn->getSystemName());
		v.push_back(&this->dbid);
	}
	if (updatenull || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (updatenull || !this->title.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TITLE=";
		sql += this->title.toValueString(conn->getSystemName());
		v.push_back(&this->title);
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
