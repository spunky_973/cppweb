#include "T_XG_DBETC.h"


void CT_XG_DBETC::clear()
{
	this->id.clear();
	this->type.clear();
	this->host.clear();
	this->port.clear();
	this->name.clear();
	this->user.clear();
	this->passwd.clear();
	this->charset.clear();
	this->enabled.clear();
	this->step.clear();
	this->stress.clear();
	this->remark.clear();
	this->statetime.clear();
}
int CT_XG_DBETC::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_DBETC(" + string(GetColumnString()) + ") VALUES(";
	sql += this->id.toValueString(conn->getSystemName());
	vec.push_back(&this->id);
	sql += ",";
	sql += this->type.toValueString(conn->getSystemName());
	vec.push_back(&this->type);
	sql += ",";
	sql += this->host.toValueString(conn->getSystemName());
	vec.push_back(&this->host);
	sql += ",";
	sql += this->port.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->name.toValueString(conn->getSystemName());
	vec.push_back(&this->name);
	sql += ",";
	sql += this->user.toValueString(conn->getSystemName());
	vec.push_back(&this->user);
	sql += ",";
	sql += this->passwd.toValueString(conn->getSystemName());
	vec.push_back(&this->passwd);
	sql += ",";
	sql += this->charset.toValueString(conn->getSystemName());
	vec.push_back(&this->charset);
	sql += ",";
	sql += this->enabled.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->step.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->stress.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_DBETC::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->id = row->getString(0);
	this->id.setNullFlag(row->isNull());
	this->type = row->getString(1);
	this->type.setNullFlag(row->isNull());
	this->host = row->getString(2);
	this->host.setNullFlag(row->isNull());
	this->port = row->getLong(3);
	this->port.setNullFlag(row->isNull());
	this->name = row->getString(4);
	this->name.setNullFlag(row->isNull());
	this->user = row->getString(5);
	this->user.setNullFlag(row->isNull());
	this->passwd = row->getString(6);
	this->passwd.setNullFlag(row->isNull());
	this->charset = row->getString(7);
	this->charset.setNullFlag(row->isNull());
	this->enabled = row->getLong(8);
	this->enabled.setNullFlag(row->isNull());
	this->step = row->getLong(9);
	this->step.setNullFlag(row->isNull());
	this->stress = row->getLong(10);
	this->stress.setNullFlag(row->isNull());
	this->remark = row->getString(11);
	this->remark.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(12);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_DBETC::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_DBETC";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_DBETC::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return find(getPKCondition(), vec);
}
string CT_XG_DBETC::getPKCondition()
{
	string condition;
	condition = "ID=";
	condition += this->id.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_DBETC::update(bool updatenull)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_DBETC SET ";
	if (updatenull || !this->type.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TYPE=";
		sql += this->type.toValueString(conn->getSystemName());
		v.push_back(&this->type);
	}
	if (updatenull || !this->host.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "HOST=";
		sql += this->host.toValueString(conn->getSystemName());
		v.push_back(&this->host);
	}
	if (updatenull || !this->port.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PORT=";
		sql += this->port.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (updatenull || !this->user.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "USER=";
		sql += this->user.toValueString(conn->getSystemName());
		v.push_back(&this->user);
	}
	if (updatenull || !this->passwd.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PASSWD=";
		sql += this->passwd.toValueString(conn->getSystemName());
		v.push_back(&this->passwd);
	}
	if (updatenull || !this->charset.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CHARSET=";
		sql += this->charset.toValueString(conn->getSystemName());
		v.push_back(&this->charset);
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->step.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STEP=";
		sql += this->step.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->stress.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STRESS=";
		sql += this->stress.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->id);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_DBETC::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_DBETC";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_DBETC::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->id);
	return remove(getPKCondition(), vec);
}
string CT_XG_DBETC::getValue(const string& key)
{
	if (key == "ID") return this->id.toString();
	if (key == "TYPE") return this->type.toString();
	if (key == "HOST") return this->host.toString();
	if (key == "PORT") return this->port.toString();
	if (key == "NAME") return this->name.toString();
	if (key == "USER") return this->user.toString();
	if (key == "PASSWD") return this->passwd.toString();
	if (key == "CHARSET") return this->charset.toString();
	if (key == "ENABLED") return this->enabled.toString();
	if (key == "STEP") return this->step.toString();
	if (key == "STRESS") return this->stress.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_DBETC::setValue(const string& key, const string& val)
{
	if (key == "ID")
	{
		this->id = val;
		return true;
	}
	if (key == "TYPE")
	{
		this->type = val;
		return true;
	}
	if (key == "HOST")
	{
		this->host = val;
		return true;
	}
	if (key == "PORT")
	{
		this->port = val;
		return true;
	}
	if (key == "NAME")
	{
		this->name = val;
		return true;
	}
	if (key == "USER")
	{
		this->user = val;
		return true;
	}
	if (key == "PASSWD")
	{
		this->passwd = val;
		return true;
	}
	if (key == "CHARSET")
	{
		this->charset = val;
		return true;
	}
	if (key == "ENABLED")
	{
		this->enabled = val;
		return true;
	}
	if (key == "STEP")
	{
		this->step = val;
		return true;
	}
	if (key == "STRESS")
	{
		this->stress = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_DBETC::update(bool updatenull, const string& condition, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_DBETC SET ";
	if (updatenull || !this->type.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TYPE=";
		sql += this->type.toValueString(conn->getSystemName());
		v.push_back(&this->type);
	}
	if (updatenull || !this->host.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "HOST=";
		sql += this->host.toValueString(conn->getSystemName());
		v.push_back(&this->host);
	}
	if (updatenull || !this->port.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PORT=";
		sql += this->port.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (updatenull || !this->user.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "USER=";
		sql += this->user.toValueString(conn->getSystemName());
		v.push_back(&this->user);
	}
	if (updatenull || !this->passwd.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "PASSWD=";
		sql += this->passwd.toValueString(conn->getSystemName());
		v.push_back(&this->passwd);
	}
	if (updatenull || !this->charset.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "CHARSET=";
		sql += this->charset.toValueString(conn->getSystemName());
		v.push_back(&this->charset);
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->step.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STEP=";
		sql += this->step.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->stress.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STRESS=";
		sql += this->stress.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
