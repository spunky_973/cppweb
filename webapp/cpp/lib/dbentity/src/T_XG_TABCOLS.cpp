#include "T_XG_TABCOLS.h"


void CT_XG_TABCOLS::clear()
{
	this->name.clear();
	this->tabid.clear();
	this->title.clear();
	this->filter.clear();
	this->remark.clear();
	this->enabled.clear();
	this->position.clear();
	this->querytype.clear();
	this->statetime.clear();
}
int CT_XG_TABCOLS::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_TABCOLS(" + string(GetColumnString()) + ") VALUES(";
	sql += this->name.toValueString(conn->getSystemName());
	vec.push_back(&this->name);
	sql += ",";
	sql += this->tabid.toValueString(conn->getSystemName());
	vec.push_back(&this->tabid);
	sql += ",";
	sql += this->title.toValueString(conn->getSystemName());
	vec.push_back(&this->title);
	sql += ",";
	sql += this->filter.toValueString(conn->getSystemName());
	vec.push_back(&this->filter);
	sql += ",";
	sql += this->remark.toValueString(conn->getSystemName());
	vec.push_back(&this->remark);
	sql += ",";
	sql += this->enabled.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->position.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->querytype.toValueString(conn->getSystemName());
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_TABCOLS::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->name = row->getString(0);
	this->name.setNullFlag(row->isNull());
	this->tabid = row->getString(1);
	this->tabid.setNullFlag(row->isNull());
	this->title = row->getString(2);
	this->title.setNullFlag(row->isNull());
	this->filter = row->getString(3);
	this->filter.setNullFlag(row->isNull());
	this->remark = row->getString(4);
	this->remark.setNullFlag(row->isNull());
	this->enabled = row->getLong(5);
	this->enabled.setNullFlag(row->isNull());
	this->position = row->getLong(6);
	this->position.setNullFlag(row->isNull());
	this->querytype = row->getLong(7);
	this->querytype.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(8);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_TABCOLS::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_TABCOLS";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_TABCOLS::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->name);
	vec.push_back(&this->tabid);
	return find(getPKCondition(), vec);
}
string CT_XG_TABCOLS::getPKCondition()
{
	string condition;
	condition = "NAME=";
	condition += this->name.toValueString(conn->getSystemName());
	condition += " AND ";
	condition += "TABID=";
	condition += this->tabid.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_TABCOLS::update(bool updatenull)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_TABCOLS SET ";
	if (updatenull || !this->title.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TITLE=";
		sql += this->title.toValueString(conn->getSystemName());
		v.push_back(&this->title);
	}
	if (updatenull || !this->filter.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "FILTER=";
		sql += this->filter.toValueString(conn->getSystemName());
		v.push_back(&this->filter);
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->position.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "POSITION=";
		sql += this->position.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->querytype.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "QUERYTYPE=";
		sql += this->querytype.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->name);
	v.push_back(&this->tabid);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_TABCOLS::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_TABCOLS";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_TABCOLS::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->name);
	vec.push_back(&this->tabid);
	return remove(getPKCondition(), vec);
}
string CT_XG_TABCOLS::getValue(const string& key)
{
	if (key == "NAME") return this->name.toString();
	if (key == "TABID") return this->tabid.toString();
	if (key == "TITLE") return this->title.toString();
	if (key == "FILTER") return this->filter.toString();
	if (key == "REMARK") return this->remark.toString();
	if (key == "ENABLED") return this->enabled.toString();
	if (key == "POSITION") return this->position.toString();
	if (key == "QUERYTYPE") return this->querytype.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_TABCOLS::setValue(const string& key, const string& val)
{
	if (key == "NAME")
	{
		this->name = val;
		return true;
	}
	if (key == "TABID")
	{
		this->tabid = val;
		return true;
	}
	if (key == "TITLE")
	{
		this->title = val;
		return true;
	}
	if (key == "FILTER")
	{
		this->filter = val;
		return true;
	}
	if (key == "REMARK")
	{
		this->remark = val;
		return true;
	}
	if (key == "ENABLED")
	{
		this->enabled = val;
		return true;
	}
	if (key == "POSITION")
	{
		this->position = val;
		return true;
	}
	if (key == "QUERYTYPE")
	{
		this->querytype = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_TABCOLS::update(bool updatenull, const string& condition, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_TABCOLS SET ";
	if (updatenull || !this->title.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "TITLE=";
		sql += this->title.toValueString(conn->getSystemName());
		v.push_back(&this->title);
	}
	if (updatenull || !this->filter.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "FILTER=";
		sql += this->filter.toValueString(conn->getSystemName());
		v.push_back(&this->filter);
	}
	if (updatenull || !this->remark.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "REMARK=";
		sql += this->remark.toValueString(conn->getSystemName());
		v.push_back(&this->remark);
	}
	if (updatenull || !this->enabled.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "ENABLED=";
		sql += this->enabled.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->position.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "POSITION=";
		sql += this->position.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->querytype.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "QUERYTYPE=";
		sql += this->querytype.toValueString(conn->getSystemName());
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
