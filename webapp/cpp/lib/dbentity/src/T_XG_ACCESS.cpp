#include "T_XG_ACCESS.h"


void CT_XG_ACCESS::clear()
{
	this->path.clear();
	this->name.clear();
	this->param.clear();
	this->menuid.clear();
	this->statetime.clear();
}
int CT_XG_ACCESS::insert()
{
	vector<DBData*> vec;

	sql = "INSERT INTO T_XG_ACCESS(" + string(GetColumnString()) + ") VALUES(";
	sql += this->path.toValueString(conn->getSystemName());
	vec.push_back(&this->path);
	sql += ",";
	sql += this->name.toValueString(conn->getSystemName());
	vec.push_back(&this->name);
	sql += ",";
	sql += this->param.toValueString(conn->getSystemName());
	vec.push_back(&this->param);
	sql += ",";
	sql += this->menuid.toValueString(conn->getSystemName());
	vec.push_back(&this->menuid);
	sql += ",";
	sql += this->statetime.toValueString(conn->getSystemName());
	sql += ")";

	return conn->execute(sql, vec);
}
bool CT_XG_ACCESS::next()
{
	if (!rs) return false;

	sp<RowData> row = rs->next();

	if (!row) return false;

	this->path = row->getString(0);
	this->path.setNullFlag(row->isNull());
	this->name = row->getString(1);
	this->name.setNullFlag(row->isNull());
	this->param = row->getString(2);
	this->param.setNullFlag(row->isNull());
	this->menuid = row->getString(3);
	this->menuid.setNullFlag(row->isNull());
	this->statetime = row->getDateTime(4);
	this->statetime.setNullFlag(row->isNull());

	return true;
}
sp<QueryResult> CT_XG_ACCESS::find(const string& condition, const vector<DBData*>& vec)
{
	sql = "SELECT " + string(GetColumnString()) + " FROM T_XG_ACCESS";

	if (condition.empty()) return rs = conn->query(sql);

	sql += " WHERE ";
	sql += condition;

	return rs = conn->query(sql, vec);
}
sp<QueryResult> CT_XG_ACCESS::find()
{
	vector<DBData*> vec;
	vec.push_back(&this->path);
	vec.push_back(&this->param);
	return find(getPKCondition(), vec);
}
string CT_XG_ACCESS::getPKCondition()
{
	string condition;
	condition = "PATH=";
	condition += this->path.toValueString(conn->getSystemName());
	condition += " AND ";
	condition += "PARAM=";
	condition += this->param.toValueString(conn->getSystemName());

	return stdx::replace(condition, "=NULL", "IS NULL");
}
int CT_XG_ACCESS::update(bool updatenull)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_ACCESS SET ";
	if (updatenull || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (updatenull || !this->menuid.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MENUID=";
		sql += this->menuid.toValueString(conn->getSystemName());
		v.push_back(&this->menuid);
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	v.push_back(&this->path);
	v.push_back(&this->param);

	sql += " WHERE " + getPKCondition();

	return conn->execute(sql, v);
}
int CT_XG_ACCESS::remove(const string& condition, const vector<DBData*>& vec)
{
	sql = "DELETE FROM T_XG_ACCESS";

	if (condition.empty()) return conn->execute(sql);

	sql += " WHERE ";
	sql += condition;

	return conn->execute(sql, vec);
}
int CT_XG_ACCESS::remove()
{
	vector<DBData*> vec;
	vec.push_back(&this->path);
	vec.push_back(&this->param);
	return remove(getPKCondition(), vec);
}
string CT_XG_ACCESS::getValue(const string& key)
{
	if (key == "PATH") return this->path.toString();
	if (key == "NAME") return this->name.toString();
	if (key == "PARAM") return this->param.toString();
	if (key == "MENUID") return this->menuid.toString();
	if (key == "STATETIME") return this->statetime.toString();

	return stdx::EmptyString();
}
bool CT_XG_ACCESS::setValue(const string& key, const string& val)
{
	if (key == "PATH")
	{
		this->path = val;
		return true;
	}
	if (key == "NAME")
	{
		this->name = val;
		return true;
	}
	if (key == "PARAM")
	{
		this->param = val;
		return true;
	}
	if (key == "MENUID")
	{
		this->menuid = val;
		return true;
	}
	if (key == "STATETIME")
	{
		this->statetime = val;
		return true;
	}

	return false;
}
int CT_XG_ACCESS::update(bool updatenull, const string& condition, const vector<DBData*>& vec)
{
	vector<DBData*> v;
	sql = "UPDATE T_XG_ACCESS SET ";
	if (updatenull || !this->name.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "NAME=";
		sql += this->name.toValueString(conn->getSystemName());
		v.push_back(&this->name);
	}
	if (updatenull || !this->menuid.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "MENUID=";
		sql += this->menuid.toValueString(conn->getSystemName());
		v.push_back(&this->menuid);
	}
	if (updatenull || !this->statetime.isNull())
	{
		if (sql.back() != ' ') sql += ",";

		sql += "STATETIME=";
		sql += this->statetime.toValueString(conn->getSystemName());
	}

	if (sql.back() == ' ') return SQLRtn_Success;

	if (condition.empty()) return conn->execute(sql, v);

	sql += " WHERE " + condition;

	for (auto& item : vec) v.push_back(item);

	return conn->execute(sql, v);
}
