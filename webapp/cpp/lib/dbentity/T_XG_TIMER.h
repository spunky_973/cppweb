#ifndef XG_ENTITY_CT_XG_TIMER_H
#define XG_ENTITY_CT_XG_TIMER_H
///////////////////////////////////////////////////////////////////////
#include <dbx/DBConnect.h>


class CT_XG_TIMER : public DBEntity
{
public:
	bool next();
	void clear();
	int insert();
	int remove();
	sp<QueryResult> find();
	string getPKCondition();
	string getValue(const string& key);
	int update(bool updatenull = false);
	bool setValue(const string& key, const string& val);
	int remove(const string& condition, const vector<DBData*>& vec = {});
	sp<QueryResult> find(const string& condition, const vector<DBData*>& vec = {});
	int update(bool updatenull, const string& condition, const vector<DBData*>& vec = {});

public:
	DBString	path;
	DBInteger	status;
	DBString	timeval;
	DBDateTime	statetime;

	static const char* GetTableName()
	{
		return "T_XG_TIMER";
	}

	static const char* GetColumnString()
	{
		return "PATH,STATUS,TIMEVAL,STATETIME";
	}
};

///////////////////////////////////////////////////////////////////////
#endif
