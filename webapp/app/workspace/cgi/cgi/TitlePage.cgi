<%@ path=${filename}%>
<%@ include="dbentity/T_XG_USER.h"%>
<%
	string user;

	try
	{
		user = checkLogin();
	}
	catch(Exception e)
	{
	}
%>

<table id='TitleTable' height='100%'>
	<tr>
<%if (user.length() > 0){%>
		<td><label class='TitleLink' id='WebSiteButton'>网址收藏</label></td>
		<td><label class='TitleLink' id='AccountButton'>用户中心</label></td>
<%}%>
		<td><label class='TitleLink' id='AboutButton'>关于我们</label></td>
		<td><label class='TitleLink' id='ShowLoginPageButton'><%=(user.empty() ? "登录" : "登出")%></label></td>
	</tr>
</table>

<script>
getVue('TitleTable');
setCurrentUser('<%=user%>');

getHttpCacheResult('/product/getwebsitelist', null, function(data){
	if (data.code > 0){
		$('#WebSiteButton').click(function(){
			var reg = new RegExp('Web', 'g');
			var msg = getHttpResult('/app/product/pub/websitepad.htm');

			showToastMessage(msg.replace(reg, 'WebDialog'), null, null, '60%', true);
			
			$('#WebDialogSiteMenuDiv').hide().parent().css('padding', '1vh 1vw');
		});
	}
	else {
		$('#WebSiteButton').remove();
	}
});

$('#AccountButton').click(function(){
	showToastMessage(getHttpResult('/app/workspace/pub/userinfo.htm'), true, null, null, true);
});

$('#AboutButton').click(function(){
	window.open('/sharenote?title=ABOUT&name=关于我们');
});

$('#ShowLoginPageButton').click(function(){
<%if (user.empty()){%>
	showLoginDialog(true);
<%}else{%>
	showConfirmMessage('&nbsp;&nbsp;是否要退出当前登录？&nbsp;&nbsp;', '退出登录', function(flag){
		if (flag){
			getHttpResult('/checklogin', {flag: 'Q'});
			clearCookie();
			updateTitle();
			updateMenu(true);
		}
	});
<%}%>
});
</script>