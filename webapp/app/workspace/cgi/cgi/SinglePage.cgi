<%@ path=${filename}%>
<%
	string user;

	param_string(path);
	param_string(icon);
	param_string(title);
	param_string(align);
	param_string(footer);
	param_string(padding);
	param_string(background);

	stdx::tolower(align);

	if (path.empty()) return XG_FAIL;

	if (icon.empty()) icon = "/favicon.ico";

	if (request->isMobile())
	{
		if (padding.empty()) padding = "1vh 3vw";
	}
	else
	{		
		if (padding.empty()) padding = "1vh 6vw";

		if (footer.empty()) footer = "/sharenote?flag=S&title=FOOTER";
	}

	if (footer == "none") footer.clear();

	if (background.empty()) background = "none";

	try
	{
		user = checkLogin();
	}
	catch(Exception e)
	{
	}
%>
<!DOCTYPE HTML>
<html>
<head>
<title><%=title%></title>
<meta name='referrer' content='always'/>
<meta name='keywords' content='<%=title%>'/>
<meta name='description' content='<%=title%>'/>
<link id='IconLink' rel='shortcut icon' href='<%=icon%>'/>
<meta http-equiv='x-ua-compatible' content='ie=edge,chrome=1'/>
<meta http-equiv='content-type' content='text/html; charset=utf-8'/>
<meta name='viewport' content='width=device-width,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no'/>

<script>
if (typeof(require) == 'function') delete window.module;
</script>

<script src='/res/lib/utils.js.gzip'></script>
<script src='/res/lib/laydate/laydate.js.gzip'></script>
<script src='/res/lib/highlight/highlight.js.gzip'></script>
<script src='/res/lib/kindeditor/kindeditor.js.gzip'></script>
<script src='/res/lib/kindeditor/lang/zh-cn.js.gzip'></script>

<link rel='stylesheet' type='text/css' href='/app/workspace/css/base.css'/>
<link rel='stylesheet' type='text/css' href='/app/workspace/css/menupad.css'/>
<link rel='stylesheet' type='text/css' href='/app/workspace/css/notepad.css'/>
<link rel='stylesheet' type='text/css' href='/res/lib/highlight/css/tomorrow.css'/>

<style>
body{
	background: <%=background%>;
	background-attachment: fixed;
}
#SinglePageDiv{
<%if (align.length() > 0){%>
	text-align: <%=align%>;
<%}%>
<%if (padding.length() > 0){%>
	padding: <%=padding%>;
<%}%>
}
#SinglePageSpan{
	text-align: left;
}
</style>

<script>
var saveneeded = null;
var automodifymap = {};
var modifyNotepad = null;

function setSaveNeeded(needed){
	saveneeded = needed;
}
function autoModifyContentSize(id){
	automodifymap[id] = id;
}

window.onload = function(){
	setCurrentUser('<%=user%>');

	$(window).resize(function(){
		if (modifyNotepad) modifyNotepad();
	});

	var path = '<%=path%>';
	var icon = '<%=icon%>';
	var title = '<%=title%>';

	var msg = getHttpResult(path);

	if (getFileExtname(path).indexOf('md') >= 0){
		marked.setOptions({
			highlight: function(code){
				return hljs.highlightAuto(code).value;
			}
		});

		msg = marked(msg);
	}
	$('#SinglePageSpan').html(msg);
<%if (footer.length() > 0){%>
	$('#SinglePageFooterDiv').load('<%=footer%>');
<%}%>
}
</script>
</head>
<body>
	<div id='SinglePageDiv'>
		<span id='SinglePageSpan'></span>
	</div>
<%if (footer.length() > 0){%>
	<div id='SinglePageFooterDiv' style='margin-top:4vh'></div>
<%}%>
</body>
</html>