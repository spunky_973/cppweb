<%@ path=${filename}%>

<%
	webx::PrintRecordview(out, "${timer}");
%>

<script>
{
	var execmoduleaccess = getAccess('execmodule?cmd=call');

	$recordvmdata.button = [{
		title: '强制执行',
		click: execTimerTask,
		disable: execmoduleaccess < 0
	}]

	$recordvmdata.filter['timeval'] = function(val){
		if (val.indexOf(':') > 0) return '每天<red>' + val + '</red>执行';
		return '每隔<red>' + val + '秒钟</red>执行一次';
	}

	function execTimerTask(item){
		var path = item.path;

		showConfirmMessage('执行定时任务[' + path + ']确认？', '执行选项', function(flag){
			if (flag){
				getHttpResult('/execmodule', {cmd: 'call', async: 1, path: path}, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code < 0){
						showToast('执行定时任务失败');
					}
					else{
						$recordvmdata.recordview.reload(false);
						showToast('执行定时任务成功');
					}
				});
			}
		});
	}

	setSingletonInterval('timerlist_timer', 3000, function(){
		$recordvmdata.recordview.reload(false);
	});
}
</script>