#include <webx/route.h>



class Notepad : public webx::ProcessBase
{
protected:
	int process();
};

int Notepad::process()
{
	param_string(id);
	param_string(name);
	param_string(level);
	param_string(title);
	param_string(folder);
	param_string(deficon);

	if (name.empty())
	{
		clearResponse();

		return XG_PARAMERR;
	}

	if (id.empty() && folder.empty())
	{
		clearResponse();

		return XG_PARAMERR;
	}
	
	try
	{
		checkLogin();
	}
	catch(Exception e)
	{
		out << "<script>sessionTimeout()</script>";

		return XG_NOTFOUND;
	}

	if (deficon.empty()) deficon = "/res/img/note/code.png";
	out<<"<style>\r\n.OutputPage{\r\n\tpadding: 0px 4px;\r\n\tword-wrap: break-word;\r\n\twhite-space: pre-wrap;\r\n}\r\n#OutputText{\r\n\toverflow: scroll;\r\n\tmargin-top: 2px;\r\n\tbackground: #FFF;\r\n\tborder: 1px solid #CCC;\r\n\tbackground: rgba(255, 255, 255, 0.8);\r\n}\r\n#CodeContentDiv{\r\n\tmargin-top: 2px;\r\n\tborder: 1px solid #DDD;\r\n}\r\n.CodeMirror{\r\n\tbackground: rgba(255, 255, 255, 0.6);\r\n}\r\n.CodeMirror-gutters{\r\n\tbackground: rgba(255, 255, 255, 0.7);\r\n}\r\n</style>\r\n\r\n<table id=\'NoteEditTable\'>\r\n\t<tr>\r\n\t\t<td id=\'NoteListTd\'>\r\n\t\t\t<div id=\'NoteListDiv\'>\r\n\t\t\t\t<table id=\'NoteListTable\'></table>\r\n\t\t\t</div>\r\n\t\t</td>\r\n\t\t<td id=\'NoteEditTd\'>\r\n\t\t\t<table>\r\n\t\t\t\t<tr>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<v-text id=\'NoteTitleText\' title=\'";
	out<<(name);
	out<<"名称\' maxlength=\'20\'></v-text>\r\n\t\t\t\t\t\t<v-select id=\'NoteLevelSelect\' title=\'";
	out<<(name);
	out<<"级别\' option=\'系统|普通|推荐|公开\'></v-select>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td style=\'padding-left:16px\'>\r\n\t\t\t\t\t\t<span title=\'点击上传";
	out<<(name);
	out<<"图标\' id=\'NoteIcon\'></span>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:24px\' id=\'SaveNoteButton\'>保存</button>\r\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:4px;color:#008800\' id=\'AddNoteButton\'>新建</button>\r\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:4px;color:#DD2233\' id=\'DeleteNoteButton\'>删除</button>\r\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:24px\' id=\'CopyNoteLinkButton\' title=\'点击复制分享链接\'>分享链接</button>\r\n\t\t\t\t\t\t<button class=\'TextButton\' style=\'margin-left:4px;color:#008800\' id=\'CompileNoteButton\' title=\'点击编译源代码\'>编译执行</button>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t\t<td>\r\n\t\t\t\t\t</td>\r\n\t\t\t\t</tr>\r\n\t\t\t</table>\r\n\t\t\t<div id=\'CodeContentDiv\'><textarea id=\'NoteContentText\'></textarea></div>\r\n\t\t\t<div id=\'OutputText\'></div>\r\n\t\t</td>\r\n\t</tr>\r\n</table>\r\n\r\n<script>\r\ngetVue(\'NoteEditTable\');";
	if (title.length() > 0 || id.length() > 0){	out<<"\t$(\'#DeleteNoteButton\').attr(\'disabled\', true);\r\n\t$(\'#NoteLevelSelect\').attr(\'disabled\', true);\r\n\t$(\'#NoteListTd\').hide();\r\n\t\r\n\t";
	if (id.empty()){	out<<"\t\t$(\'#NoteTitleText\').attr(\'disabled\', true);\r\n\t";
	}	}	out<<"var param = null;\r\nvar curid = null;\r\nvar notepad = null;\r\nvar curicon = null;\r\nvar curtitle = null;\r\nvar curlevel = null;\r\nvar titlelist = null;\r\nvar uploadicon = null;\r\nvar curcontent = null;\r\nvar curiconbtn = null;\r\nvar curnoteitem = null;\r\nvar curdatetime = null;\r\nvar selnoteitem = null;\r\nvar uploadimage = null;\r\n\r\ninitNotepad();\r\n\r\n$(\'#NoteLevelSelect option:first\').attr(\'disabled\', true);\r\n\r\nvar menubox = new ContextMenu(\'NoteListDiv\', [\'前移\', \'后移\'], function(text, elem){\r\n\tlet arr = [];\r\n\tlet item = $.pack(getContextMenuView());\r\n\r\n\tif (text == \'前移\'){\r\n\t\titem.prev().insertAfter(item);\r\n\t}\r\n\telse{\r\n\t\titem.next().insertBefore(item);\r\n\t}\r\n\r\n\titem.parent().children().each(function(){\r\n\t\tarr.push($(this).children(\'td:last\').children(\'font:first\').text());\r\n\t});\r\n\r\n\tgetHttpResult(\'/compile/editnote\', {flag: \'M\', folder: \'";
	out<<(folder);
	out<<"\', title: arr.join(\',\')});\r\n});\r\n\r\nuploadimage = new UploadImageWidget(\'NoteIcon\', \'";
	out<<(name);
	out<<"图标\', \'14px\', 1024 * 1024, true);\r\n\r\nuploadimage.callback(function(data){\r\n\tif (data == null || data.code < 0){\r\n\t\tshowErrorToast(\'图片上传失败\');\r\n\t}\r\n\telse{\r\n\t\tuploadicon = data.url;\r\n\t}\r\n});\r\n\r\nuploadimage.image.parent().click(function(){\r\n\tvar iconpick = new IconPicker(uploadimage.image, \'note\');\r\n\t\r\n\tuploadimage.image.blur(function(){\r\n\t\tuploadicon = true;\r\n\t\ticonpick.hide();\r\n\t});\r\n});\r\n\r\n$(\'#CompileNoteButton\').click(function(){\r\n\tcompile();\r\n});\r\n\r\nfunction compile(){\r\n\tvar text = notepad.getValue();\r\n\tvar output = $(\'#OutputText\');\r\n\r\n\tif (strlen(text) == 0) return notepad.focus();\r\n\r\n\tshowToastMessage(\'正在编译代码...\');\r\n\r\n\toutput.html(\'\');\r\n\t\r\n\tgetHttpResult(\'/compile/compile\', {code: text}, function(data){\r\n\t\thideToastBox();\r\n\r\n\t\tmodifyNotepad(true);\r\n\r\n\t\tif (data.code == XG_AUTHFAIL){\r\n\t\t\tshowNoAccessToast();\r\n\t\t}\r\n\t\telse if (data.code == XG_PARAMERR){\r\n\t\t\tif (data.output){\r\n\t\t\t\toutput.html(\"<font color=\'#EE0000\'><pre class=\'OutputPage\'>\" + data.output + \"</pre></font>\");\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\toutput.html(\"<font color=\'#EE0000\'><pre class=\'OutputPage\'>源码编译失败</pre></font>\");\r\n\t\t\t}\r\n\t\t}\r\n\t\telse if (data.code == XG_TIMEOUT){\r\n\t\t\toutput.html(\"<font color=\'#EE0000\'><pre class=\'OutputPage\'>执行超时(源码中可能存在耗时操作)</pre></font>\");\r\n\t\t}\r\n\t\telse if (data.code == XG_SYSBUSY){\r\n\t\t\tif (idx++ < 50){\r\n\t\t\t\tcompile();\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\tshowToast(\'系统繁忙---请稍后再试\');\r\n\r\n\t\t\t\tidx = 0;\r\n\t\t\t}\r\n\t\t}\r\n\t\telse if (data.code < 0){\r\n\t\t\tif (data.status && data.status == 403){\r\n\t\t\t\tshowToast(\'访问频繁请稍后再试\');\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\tshowToast(\'源码编译失败\');\r\n\t\t\t}\r\n\t\t}\r\n\t\telse{\r\n\t\t\toutput.html(\"<pre class=\'OutputPage\'>\" + data.output + \"</pre>\");\r\n\t\t}\r\n\t}, true);\r\n}\r\n\r\nfunction modifyNotepad(flag){\r\n\tvar width = getClientWidth();\r\n\tvar height = getClientHeight();\r\n\r\n\tvar w = 0;\r\n\tvar h = 0;\r\n\tvar cx = width - 200;\r\n\tvar cy = height - 97;\r\n\r\n\tif (getSideWidth) cx -= getSideWidth();\r\n\t\r\n\t$(\'#NoteListDiv\').height(cy + 28);\r\n\r\n\tif (flag){\r\n\t\tw = cx;\r\n\t\th = 160;\r\n\t\tcy = cy -  h - 5;\r\n\t\t\r\n\t\t$(\'#OutputText\').width(w).height(h).show();\r\n\t}\r\n\telse{\r\n\t\t$(\'#OutputText\').hide();\r\n\t}\r\n\r\n\t$(\'#CodeContentDiv\').width(cx).height(cy);\r\n\r\n\tnotepad.setSize(\'100%\',\'100%\');\r\n}\r\nfunction initNotepad(){\r\n\tnotepad = CodeMirror.fromTextArea(document.getElementById(\'NoteContentText\'), {\r\n\t\tmode: \'text/x-csrc\',\r\n\t\tonBlur: notepadBlur,\r\n\t\ttabSize: 4,\r\n\t\tindentUnit: 4,\r\n\t\tinputStyle: \'textarea\',\r\n\t\tfoldGutter: true,\r\n\t\tlineNumbers: true,\r\n\t\tlineWrapping: true,\r\n\t\tmatchBrackets: true,\r\n\t\tindentWithTabs: true,\r\n\t\tstyleActiveLine: true,\r\n\t\tshowCursorWhenSelecting: true,\r\n\t\tgutters: [\'CodeMirror-linenumbers\', \'CodeMirror-foldgutter\']\r\n\t});\r\n\t\r\n\tmodifyNotepad(false);\r\n\r\n\tnotepad.on(\'blur\', notepadBlur);\r\n\r\n\treturn notepad;\r\n}\r\n\r\nfunction addNoteResult(flag){\r\n\tif (flag == 1) return editNoteItem(param, \'A\');\r\n}\r\n\r\nfunction delNoteResult(flag){\r\n\tif (flag == 1) return editNoteItem(param, \'D\');\r\n}\r\n\r\nfunction delNoteFolderResult(flag){\r\n\tif (flag == 1) return editNoteItem(param, \'R\');\r\n}\r\n\r\nfunction getContent(msg){\r\n\treturn notepad ? notepad.getValue() : null;\r\n}\r\n\r\nfunction setContent(msg){\r\n\tnotepad.setValue(msg);\r\n\tmodifyNotepad(false);\r\n\tsetNotepadFocus();\r\n}\r\n\r\nfunction setNotepadFocus(){\r\n\tnotepadBlur(notepad);\r\n}\r\n\r\nfunction notepadBlur(notepad){\r\n\tvar content = getContent();\r\n\tif (content != (curcontent || \'\')){\r\n\t\tsetSaveNeeded(function(func){\r\n\t\t\tvar icon = getBackgroundImage(uploadimage.image[0]);\r\n\t\t\tvar level = $(\'#NoteLevelSelect\').val();\r\n\t\t\tvar title = $(\'#NoteTitleText\').val();\r\n\r\n\t\t\tsetSaveNeeded(null);\r\n\r\n\t\t\tparam = {};\r\n\t\t\tparam[\'id\'] = curid;\r\n\t\t\tparam[\'level\'] = level;\r\n\t\t\tparam[\'title\'] = title;\r\n\t\t\tparam[\'content\'] = content;\r\n\t\t\tparam[\'folder\'] = \'";
	out<<(folder);
	out<<"\';\r\n\t\t\t\r\n\t\t\tif ((len = strlen(title)) == 0){\r\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"名称不能为空\');\r\n\t\t\t\t$(\'#NoteTitleText\').focus();\r\n\t\t\t}\r\n\t\t\telse if (len > 24){\r\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"名称最多12个汉字或24个英文字母\');\r\n\t\t\t\t$(\'#NoteTitleText\').focus();\r\n\t\t\t}\r\n\t\t\telse if ((len = strlen(content)) == 0){\r\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"内容不能为空\');\r\n\t\t\t\tsetNotepadFocus();\r\n\t\t\t}\r\n\t\t\telse if (len > 1024 * 1024){\r\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"内容太长\');\r\n\t\t\t\tsetNotepadFocus();\r\n\t\t\t}\r\n\t\t\telse\r\n\t\t\t{\r\n\t\t\t\tparam[\'icon\'] = getString(icon);\r\n\r\n\t\t\t\tif (title == curtitle){\r\n\t\t\t\t\tshowConfirmMessage(\'";
	out<<(name);
	out<<"内容已修改，是否马上保存？\', \'保存选项\', function(flag){\r\n\t\t\t\t\t\tif (flag == 1) editNoteItem(param, \'U\');\r\n\t\t\t\t\t\tif (func) func();\r\n\t\t\t\t\t});\r\n\t\t\t\t}\r\n\t\t\t\telse{";
	if (title.length() > 0 || id.length() > 0){	out<<"\t\t\t\t\tshowConfirmMessage(\'";
	out<<(name);
	out<<"内容已修改，是否马上保存？\', \'保存选项\', function(flag){\r\n\t\t\t\t\t\tif (flag == 1) editNoteItem(param, \'U\');\r\n\t\t\t\t\t\tif (func) func();\r\n\t\t\t\t\t});";
	}else{	out<<"\t\t\t\t\tshowConfirmMessage(\'";
	out<<(name);
	out<<"名称已修改，是否新建";
	out<<(name);
	out<<"？\', \'是否新建\', function(flag){\r\n\t\t\t\t\t\tif (flag == 1) editNoteItem(param, \'A\');\r\n\t\t\t\t\t\tif (func) func();\r\n\t\t\t\t\t});";
	}	out<<"\t\t\t\t}\r\n\r\n\t\t\t\treturn true;\r\n\t\t\t}\r\n\t\t\t\r\n\t\t\tif (func) func();\r\n\t\t\t\r\n\t\t\treturn false;\r\n\t\t});\r\n\t}\r\n}\r\n\r\nfunction loadNoteItem(flag){\r\n\t$(\'#NoteEditTable\').hide();\r\n\r\n\tparam = {};\r\n\tparam[\'id\'] = \'";
	out<<(id);
	out<<"\';\r\n\tparam[\'level\'] = \'";
	out<<(level);
	out<<"\';\r\n\tparam[\'title\'] = \'";
	out<<(title);
	out<<"\';\r\n\tparam[\'folder\'] = \'";
	out<<(folder);
	out<<"\';\r\n\r\n\t$(\'#CopyNoteLinkButton\').attr(\'disabled\', true);\r\n\t$(\'#NoteLevelSelect\').val(1);\r\n\ttitlelist = \'|\';\r\n\r\n\tgetHttpResult(\'/compile/getnotelist\', param, function(data){\r\n\t\t$(\'#NoteListTable\').html(\'\');\r\n\r\n\t\tif (data.code == XG_TIMEOUT){\r\n\t\t\tsessionTimeout();\r\n\t\t}\r\n\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\tshowNoAccessToast();\r\n\t\t}\r\n\t\telse if (data.code < 0){\r\n\t\t\tshowToast(\'加载数据失败\');\r\n\t\t}\r\n\t\telse if (data.code == 0){";
	if (title.length() > 0 || id.length() > 0){	out<<"\t\t\tshowToast(\'没有找到";
	out<<(name);
	out<<"数据\');";
	}else if (folder == "WEBPAGE"){	out<<"\t\t\t$(\'#NoteLevelSelect\').val(3);\r\n\t\t\t$(\'#DeleteNoteButton\').attr(\'disabled\', true);\r\n\t\t\tif (flag == \'D\'){\r\n\t\t\t\tcurid = null;\r\n\t\t\t}";
	}else{	out<<"\t\t\tif (flag == \'D\'){\r\n\t\t\t\tshowConfirmMessage(\'目录[\' + \'";
	out<<(folder);
	out<<"\' + \']\' + \'无";
	out<<(name);
	out<<"信息。<br>是否决定删除该目录？\', \'删除目录\', delNoteFolderResult);\r\n\t\t\t\tcurid = null;\r\n\t\t\t\tflag = false;\r\n\t\t\t}";
	}	out<<"\t\t\tuploadimage.image.css(\'background-image\', \'url(";
	out<<(deficon);
	out<<")\');\r\n\t\t\t$(\'#NoteTitleText\').val(\'\');\r\n\t\t\t$(\'#NoteEditTable\').show();\r\n\t\t\tsetContent(\'\');\r\n\t\t}\r\n\t\telse{\r\n\t\t\tcurdatetime = null;\r\n\t\t\tselnoteitem = null;\r\n\r\n\t\t\t$.each(data.list, function(idx, item){\r\n\t\t\t\ttitlelist += item.title + \'|\';\r\n\t\t\t\taddNoteItem(item.id, item.title, item.icon, item.level, item.statetime);\r\n\t\t\t});\r\n\t\t\t\r\n\t\t\tselectNoteItem(selnoteitem);\r\n\t\t\t\r\n\t\t\t$(\".NotepadItem\").each(function(){\r\n\t\t\t\tmenubox.bind(this);\r\n\t\t\t});\r\n\t\t}\r\n\t});\r\n\r\n\treturn flag;\r\n}\r\nfunction updateNoteInfo(note, level, title){\r\n\tlevel = getString(level);\r\n\ttitle = \'<font>\' + title + \'</font>\';\r\n\t\r\n\tif (level == \'0\'){\r\n\t\tnote.children().last().html(title + \'<span>系统</span>\').children(\'span\').css(\'color\', \'#EE2233\');\r\n\t}\r\n\telse if (level ==\'1\'){\r\n\t\tnote.children().last().html(title + \'<span>普通</span>\').children(\'span\').css(\'color\', \'#000000\');\r\n\t}\r\n\telse if (level ==\'2\'){\r\n\t\tnote.children().last().html(title + \'<span>推荐</span>\').children(\'span\').css(\'color\', \'#000000\').css(\'font-weight\', \'bold\');\r\n\t}\r\n\telse{\r\n\t\tnote.children().last().html(title + \'<span>公开</span>\').children(\'span\').css(\'color\', \'#22BB22\');\r\n\t}\r\n}\r\nfunction addNoteItem(id, title, icon, level, statetime){\r\n\t$(\'#NoteListTable\').append(\"<tr class=\'NotepadItem\' id=\'Note\" + id + \"\' value=\'\" + id + \"\'><td class=\'NoteIconButton\' style=\'background-image:url(\" + icon + \")\'></td><td></td></tr>\");\r\n\t\r\n\tvar note = $(\'.NotepadItem\').last();\r\n\t\r\n\tif (curdatetime == null || curdatetime < statetime || selnoteitem == null){\r\n\t\tcurdatetime = statetime;\r\n\t\tselnoteitem = note;\r\n\t}\r\n\t\r\n\tupdateNoteInfo(note, level, title);\r\n\r\n\treturn note.click(function(){\r\n\t\tselectNoteItem($(this));\r\n\t});\r\n}\r\nfunction editNoteItem(param, flag){\r\n\tif (flag == \'A\') param[\'id\'] = \'\';\r\n\r\n\tif (flag == \'C\' || flag == \'D\' || flag == \'R\'){\r\n\t\tparam[\'title\'] = \'\';\r\n\t\tparam[\'content\'] = \'\';\r\n\t}\r\n\r\n\tparam[\'flag\'] = flag;\r\n\tparam[\'deficon\'] = \'";
	out<<(deficon);
	out<<"\';\r\n\t\r\n\tif (flag == \'U\' && uploadicon == null) param[\'icon\'] = \'\';\r\n\r\n\tgetHttpResult(\'/compile/editnote\', param, function(data){\r\n\t\tif (data.code == XG_TIMEOUT){\r\n\t\t\tsessionTimeout();\r\n\t\t}\r\n\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\tshowNoAccessToast();\r\n\t\t}\r\n\t\telse if (data.code < 0){\r\n\t\t\tif (flag == \'A\'){\r\n\t\t\t\tshowToast(\'新建";
	out<<(name);
	out<<"失败\');\r\n\t\t\t}\r\n\t\t\telse if (flag == \'D\'){\r\n\t\t\t\tshowToast(\'删除";
	out<<(name);
	out<<"失败\');\r\n\t\t\t}\r\n\t\t\telse if (flag == \'U\'){\r\n\t\t\t\tshowToast(\'修改";
	out<<(name);
	out<<"失败\');\r\n\t\t\t}\r\n\t\t\telse if (flag == \'R\'){\r\n\t\t\t\tshowToast(\'删除目录失败\');\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\tshowToast(\'保存";
	out<<(name);
	out<<"失败\');\r\n\t\t\t}\r\n\t\t}\r\n\t\telse{\r\n\t\t\tsetSaveNeeded(null);\r\n\t\t\t\r\n\t\t\tif (flag == \'R\'){\r\n\t\t\t\tselectMenu(curmenuitem, true);\r\n\t\t\t}\r\n\t\t\telse if (flag == \'A\'){\r\n\t\t\t\tshowToast(\'新建";
	out<<(name);
	out<<"成功\');\r\n\t\t\t\ttitlelist += param[\'title\'] + \'|\';\r\n\t\t\t\tselectNoteItem(addNoteItem(data.id, param[\'title\'], data.icon, param[\'level\'], data.statetime));\r\n\t\t\t\tmenubox.bind(\'Note\' + data.id);\r\n\t\t\t}\r\n\t\t\telse if (flag == \'U\'){\r\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"保存成功\');\r\n\t\t\t\t\r\n\t\t\t\tif (curtitle != param[\'title\']){\r\n\t\t\t\t\tvar pos = titlelist.indexOf(\'|\' + curtitle + \'|\');\r\n\t\t\t\t\tif (pos >= 0){\r\n\t\t\t\t\t\ttitlelist = titlelist.substring(0, pos) + param[\'title\'] + titlelist.substring(pos + curtitle.length);\r\n\t\t\t\t\t}\r\n\t\t\t\t}\r\n\t\t\t\t\r\n\t\t\t\tcurcontent = param[\'content\'] || curcontent;\r\n\t\t\t\tcurtitle = param[\'title\'];\r\n\t\t\t\tcurlevel = param[\'level\'];\r\n\t\t\t\tcuricon = param[\'icon\'];\r\n\r\n\t\t\t\tif (strlen(data.icon) > 0){\r\n\t\t\t\t\tuploadimage.image.css(\'background-image\', \'url(\' + data.icon + \')\');\r\n\t\t\t\t\tcuricon = data.icon;\r\n\t\t\t\t}\r\n\r\n\t\t\t\tif (strlen(curicon) > 0){\r\n\t\t\t\t\tcuriconbtn.css(\'background-image\', \'url(\' + curicon + \')\');\r\n\t\t\t\t}\r\n\t\t\t\t\r\n\t\t\t\tupdateNoteInfo(curnoteitem, curlevel, curtitle);\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\tflag = loadNoteItem(flag);\r\n\t\t\t}\r\n\t\t}\r\n\t});\r\n\t\r\n\treturn flag;\r\n}\r\nfunction selectNoteItem(note){\r\n\tif (saveneeded){\r\n\t\tsaveneeded(function(){\r\n\t\t\tselectNoteItem(note);\r\n\t\t});\r\n\t\t\r\n\t\tsaveneeded = null;\r\n\t\treturn true;\r\n\t}\r\n\r\n\t$(\'.NotepadItem\').css(\'background\', \'none\');\r\n\tnote.css(\'backgroundColor\', \'rgba(0, 0, 0, 0.3)\');\r\n\tcuriconbtn = note.find(\'.NoteIconButton\');\r\n\tid = note.attr(\'value\');\r\n\tcurnoteitem = note;\r\n\tuploadicon = null;\r\n\tcurcontent = null;\r\n\tcurtitle = null;\r\n\tcurlevel = null;\r\n\tcurid = null;\r\n\r\n\tif (strlen(id) == 0){\r\n\t\tselectMenu(curmenuitem, true);\r\n\t}\r\n\telse{\r\n\t\tshowToastMessage(\'正在加载数据...\');\r\n\t\t$(\'#NoteTitleText\').val(\'\');\r\n\t\tparam = {};\r\n\t\tparam[\'id\'] = id;\r\n\t\tgetHttpResult(\'/compile/getnotecontent\', param, function(data){\r\n\t\t\thideToastBox();\r\n\t\t\tif (data.code == XG_TIMEOUT){\r\n\t\t\t\tsessionTimeout();\r\n\t\t\t}\r\n\t\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\t\tshowNoAccessToast();\r\n\t\t\t}\r\n\t\t\telse if (data.code < 0){\r\n\t\t\t\tshowToast(\'加载数据失败\');\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\tcurcontent = data.content;\r\n\t\t\t\tcurtitle = data.title;\r\n\t\t\t\tcurlevel = data.level;\r\n\t\t\t\tcuricon = data.icon;\r\n\r\n\t\t\t\tuploadimage.image.css(\'background-image\', \'url(\' + data.icon + \')\');\r\n\t\t\t\t$(\'#CopyNoteLinkButton\').removeAttr(\'disabled\');\r\n\t\t\t\t$(\'#NoteLevelSelect\').val(curlevel);\r\n\t\t\t\t$(\'#NoteTitleText\').val(curtitle);\r\n\t\t\t\t$(\'#NoteEditTable\').show();\r\n\r\n\t\t\t\tsetContent(curcontent);\r\n\r\n\t\t\t\tif (curid == null && navigator.userAgent.toLowerCase().indexOf(\"firefox\") >= 0){\r\n\t\t\t\t\tnotepad.fullscreen(true);\r\n\t\t\t\t\tnotepad.fullscreen(false);\r\n\t\t\t\t}\r\n\t\t\t\t\r\n\t\t\t\tcurid = data.id;\r\n\t\t\t\t";
	if (title.empty() && id.empty()){	out<<"\t\t\t\tif (curlevel == 0){\r\n\t\t\t\t\t$(\'#DeleteNoteButton\').attr(\'disabled\', true);\r\n\t\t\t\t\t$(\'#NoteLevelSelect\').attr(\'disabled\', true);\r\n\t\t\t\t\t$(\'#NoteTitleText\').attr(\'disabled\', true);\r\n\t\t\t\t}\r\n\t\t\t\telse{\r\n\t\t\t\t\t$(\'#DeleteNoteButton\').removeAttr(\'disabled\');\r\n\t\t\t\t\t$(\'#NoteLevelSelect\').removeAttr(\'disabled\');\r\n\t\t\t\t\t$(\'#NoteTitleText\').removeAttr(\'disabled\');\r\n\t\t\t\t\t$(\'#NoteIcon\').removeAttr(\'disabled\');\r\n\t\t\t\t}";
	}	out<<"\t\t\t}\r\n\t\t}, true);\r\n\t}\r\n}\r\n\r\n$(\'#DeleteNoteButton\').click(function(){\r\n\tif (curid == null){\r\n\t\tshowConfirmMessage(\'目录[\' + \'";
	out<<(folder);
	out<<"\' + \']\' + \'无";
	out<<(name);
	out<<"信息。<br>是否决定删除该目录？\', \'删除目录\', delNoteFolderResult);\r\n\t}\r\n\telse{\r\n\t\tparam = {};\r\n\t\tparam[\'id\'] = curid;\r\n\t\tshowConfirmMessage(\'是否决定删除当前";
	out<<(name);
	out<<"？\', \'删除";
	out<<(name);
	out<<"\', delNoteResult);\r\n\t}\r\n});\r\n\r\n$(\'#AddNoteButton\').click(function(){\r\n\tvar uploadbutton = null;\r\n\r\n\tshowConfirmMessage(\"<table id=\'AddNoteTable\' class=\'DialogTable\'><tr><td><v-select id=\'AddNoteLevelSelect\' title=\'";
	out<<(name);
	out<<"级别\' option=\'系统|普通|推荐|公开\'></v-select></td></tr><tr><td><v-text id=\'AddNoteTitleText\' title=\'";
	out<<(name);
	out<<"名称\' size=\'14\' maxlength=\'20\'></v-text></td></tr></table>\", \'添加";
	out<<(name);
	out<<"\', function(flag){\r\n\t\tif (flag == 0) return true;\r\n\r\n\t\tvar title = $(\'#AddNoteTitleText\').val();\r\n\t\tvar level = $(\'#AddNoteLevelSelect\').val();\r\n\r\n\t\tif ((len = strlen(title)) == 0){\r\n\t\t\t$(\'#AddNoteTitleText\').focus();\r\n\t\t\treturn false;\r\n\t\t}\r\n\t\telse if (len > 24){\r\n\t\t\tsetMessageErrorText(\'";
	out<<(name);
	out<<"名称太长\', $(\'#AddNoteTitleText\'));\r\n\t\t\treturn false;\r\n\t\t}\r\n\t\telse if (!isFileName(title)){\r\n\t\t\tsetMessageErrorText(\'名称不能有特殊字符\', $(\'#AddNoteTitleText\'));\r\n\t\t\treturn false;\r\n\t\t}\r\n\t\telse if (titlelist.indexOf(\'|\' + title + \'|\') >= 0){\r\n\t\t\tsetMessageErrorText(\'名称与现有";
	out<<(name);
	out<<"冲突\', $(\'#AddNoteTitleText\'));\r\n\t\t\treturn false;\r\n\t\t}\r\n\t\t\r\n\t\tparam = {};\r\n\t\tparam[\'level\'] = level;\r\n\t\tparam[\'title\'] = title;\r\n\t\tparam[\'folder\'] = \'";
	out<<(folder);
	out<<"\';\r\n\r\n\t\treturn editNoteItem(param, \'A\');\r\n\t});";
	if (folder == "WEBPAGE"){	out<<"\t$(\'#AddNoteLevelSelect\').val(3);";
	}else{	out<<"\t$(\'#AddNoteLevelSelect\').val(1).children(\'option\').first().attr(\'disabled\', true);";
	}	out<<"\t$(\'#AddMenuTitleText\').focus();\r\n});\r\n\r\n$(\'#SaveNoteButton\').click(function(){\r\n\tvar icon = getBackgroundImage(uploadimage.image[0]);\r\n\tvar level = $(\'#NoteLevelSelect\').val();\r\n\tvar title = $(\'#NoteTitleText\').val();\r\n\tvar content = getContent();\r\n\r\n\tsetSaveNeeded(null);\r\n\r\n\tparam = {};\r\n\tparam[\'id\'] = curid;\r\n\tparam[\'level\'] = level;\r\n\tparam[\'title\'] = title;\r\n\tparam[\'content\'] = content;\r\n\tparam[\'folder\'] = \'";
	out<<(folder);
	out<<"\';\r\n\t\r\n\tif ((len = strlen(title)) == 0){\r\n\t\tshowToast(\'";
	out<<(name);
	out<<"名称不能为空\');\r\n\t\t$(\'#NoteTitleText\').focus();\r\n\t}\r\n\telse if (len > 24){\r\n\t\tshowToast(\'";
	out<<(name);
	out<<"名称最多12个汉字或24个英文字母\');\r\n\t\t$(\'#NoteTitleText\').focus();\r\n\t}\r\n\telse if ((len = strlen(content)) == 0){\r\n\t\tshowToast(\'";
	out<<(name);
	out<<"内容不能为空\');\r\n\t\tsetNotepadFocus();\r\n\t}\r\n\telse if (len > 1024 * 1024){\r\n\t\tshowToast(\'";
	out<<(name);
	out<<"内容太长，请分段保存\');\r\n\t\tsetNotepadFocus();\r\n\t}\r\n\telse{\r\n\t\tif (uploadicon == null) icon = curicon;\r\n\r\n\t\tparam[\'icon\'] = getString(icon);\r\n\r\n\t\tif (strlen(curid) == 0){\r\n\t\t\teditNoteItem(param, \'A\');\r\n\t\t}\r\n\t\telse if (title == curtitle){\r\n\t\t\tif (icon == curicon && level == curlevel && content == curcontent){\r\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"内容未更新\');\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\tif (content == curcontent) delete param[\'content\'];\r\n\t\t\t\teditNoteItem(param, \'U\');\r\n\t\t\t}\r\n\t\t}\r\n\t\telse{\r\n\t\t\tif (titlelist.indexOf(\'|\' + title + \'|\') >= 0){\r\n\t\t\t\tshowToast(\'";
	out<<(name);
	out<<"名称与现有";
	out<<(name);
	out<<"冲突\');\r\n\t\t\t\t$(\'#NoteTitleText\').focus();\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\teditNoteItem(param, \'U\');\r\n\t\t\t}\r\n\t\t}\r\n\t}\r\n});\r\n\r\n$(\'#CopyNoteLinkButton\').click(function(){\r\n\tvar clipboard = new Clipboard(\'#CopyNoteLinkButton\', {\r\n\t\ttext: function(){\r\n\t\t\tvar host = window.location.href;\r\n\t\t\tvar link = \'/compile/mainframe?path=/compile/sharenote\' + encodeURIComponent(\'?flag=S&dbid=";
	out<<(token->getDataId());
	out<<"&id=\' + curid) + \'&title=\' + encodeURIComponent(curtitle);\r\n\t\t\tif (strlen(curicon) < 128) link += \'&icon=\' + curicon;\r\n\t\t\tshowToast(\"<a class=\'TextLink\' href=\'\" + link + \"\' target=\'_blank\'>";
	out<<(name);
	out<<"分享链接已经复制到剪切板</a>\", 3000);\r\n\t\t\treturn host.indexOf(\'/\', 8) > 0 ? host.substr(0, host.indexOf(\'/\', 8)) + link : host + link;\r\n\t\t}\r\n\t});\r\n});\r\n\r\nloadNoteItem();\r\n</script>";


	return XG_OK;
}
HTTP_WEBAPP(Notepad, CGI_PRIVATE, "/compile/Notepad")