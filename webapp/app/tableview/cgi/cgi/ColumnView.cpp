#include <webx/route.h>



class ColumnView : public webx::ProcessBase
{
protected:
	int process();
};

int ColumnView::process()
{
	param_string(flag);
	param_string(name);
	param_string(title);
	param_string(tabid);
	param_string(filter);
	param_string(remark);
	param_string(querytype);

	checkLogin();

	auto add = [&](){
		param_string(column);

		auto vec = stdx::split(column, ",");
		auto dbconn = webx::GetDBConnect();
		
		for (int i = 0; i < vec.size(); i++)
		{
			const string& field = vec[i];

			if (name == field)
			{
				dbconn->execute("INSERT INTO T_XG_TABCOLS(NAME,TABID,TITLE,FILTER,REMARK,QUERYTYPE,POSITION,ENABLED,STATETIME) VALUES(?,?,?,?,?,?,?,2,?)", name, tabid, title, filter, remark, querytype, i, DateTime::ToString());
			}
			else
			{
				dbconn->execute("UPDATE T_XG_TABCOLS SET position=? WHERE TABID=? AND NAME=? AND ENABLED>1", i, tabid, field);
			}
		}

	
		return XG_OK;
	};

	auto remove = [&](){
		return webx::GetDBConnect()->execute("DELETE FROM T_XG_TABCOLS WHERE TABID=? AND NAME=? AND ENABLED>1", tabid, name);
	};

	auto update = [&](){
		return webx::GetDBConnect()->execute("UPDATE T_XG_TABCOLS SET TITLE=?,REMARK=?,FILTER=?,QUERYTYPE=?,STATETIME=? WHERE TABID=? AND NAME=? AND ENABLED>1", title, remark, filter, querytype, DateTime::ToString(), tabid, name);
	};

	if (flag.length() > 0)
	{
		int res = XG_OK;

		if (flag == "A")
		{
			res = add();
		}
		else if (flag == "D")
		{
			res = remove();
		}
		else if (flag == "U")
		{
			res = update();
		}

		return simpleResponse(res);
	}
	out<<"<style>\r\n#RecordColumnDiv{\r\n\tmin-height: 60vh;\r\n\tbackground: #CCC;\r\n}\r\n#RecordColumnDiv table{\r\n\tmin-width: 60vw;\r\n}\r\n#RecordColumnDiv table td{\r\n\tpadding: 5px 4px;\r\n}\r\n#RecordColumnDiv table button{\r\n\tcolor: #090;\r\n\tmargin: 0px 3px;\r\n\tpadding: 1px 2px;\r\n}\r\n#RecordColumnDiv table tr:first-child td{\r\n\tfont-size: 0.95rem;\r\n\tfont-weight: bold;\r\n}\r\n#RecordColumnDiv table tr:nth-child(even){\r\n\tbackground: rgba(180, 180, 180, 0.5);\r\n}\r\n#RecordColumnDiv table tr:nth-child(odd){\r\n\tbackground:rgba(100, 160, 200, 0.5);\r\n}\r\n#RecordColumnDiv table tr:first-child{\r\n\tbackground: rgba(80, 0, 80, 0.5);\r\n}\r\n</style>\r\n\r\n<div id=\'RecordColumnDiv\'>\r\n\t<table>\r\n\t\t<tr>\r\n\t\t\t<td>字段名</td>\r\n\t\t\t<td>字段标题</td>\r\n\t\t\t<td>查询类型</td>\r\n\t\t\t<td>过滤条件</td>\r\n\t\t\t<td>字段说明</td>\r\n\t\t\t<td><button @click=\'addColumn(-1)\' class=\'TextButton\'>添加</button></td>\r\n\t\t</tr>\r\n\t\t<tr v-for=\'(item,index) in colist\'>\r\n\t\t\t<td>{{item.name}}</td>\r\n\t\t\t<td>{{item.title}}</td>\r\n\t\t\t<td>{{item.querytype}}</td>\r\n\t\t\t<td>{{item.filter}}</td>\r\n\t\t\t<td>{{item.remark}}</td>\r\n\t\t\t<td>\r\n\t\t\t\t<button @click=\'addColumn(index)\' class=\'TextButton\'>添加</button>\r\n\t\t\t\t<button @click=\'updateColumn(item)\' class=\'TextButton\' style=\'color:#009\'>编辑</button>\r\n\t\t\t\t<button @click=\'removeColumn(item)\' class=\'TextButton\' style=\'color:#C00\'>删除</button>\r\n\t\t\t</td>\r\n\t\t</tr>\r\n\t</table>\r\n</div>\r\n\r\n<script>\r\n{\r\n\tlet querytypelist = [\'不用查询\', \'完全匹配\', \'前缀匹配\', \'模糊匹配\', \'日期范围\', \'时间范围\'];\r\n\r\n\tlet vmdata = {\r\n\t\tcolist: [],\r\n\t\tcodata: {\r\n\t\t\ttitle: [\'查询条件\', \'字段名\', \'字段标题\', \'过滤条件\', \'字段说明\'],\r\n\t\t\tmodel: {querytype: \'不用查询\', name: \'\', title: \'\', filter: \'\', remark: \'\'},\r\n\t\t\tstyle: [\r\n\t\t\t\t{select: querytypelist},\r\n\t\t\t\t{size: 24, minlength: 1, maxlength: 32, filter: commonfilter.name},\r\n\t\t\t\t{size: 24, minlength: 1, maxlength: 32},\r\n\t\t\t\t{size: 24, minlength: 0, maxlength: 256, type: \'textarea\'},\r\n\t\t\t\t{size: 24, minlength: 0, maxlength: 256, type: \'textarea\'}\r\n\t\t\t]\r\n\t\t}\r\n\t}\r\n\r\n\tgetVue(\'RecordColumnDiv\', vmdata);\r\n\r\n\tfunction loadColumnList(){\r\n\t\tgetHttpResult(\'/getcolumnlist\', {tabid: \'";
	out<<(tabid);
	out<<"\'}, function(data){\r\n\t\t\tif (data.code > 0){\r\n\t\t\t\tfor (var i = 0; i < data.list.length; i++){\r\n\t\t\t\t\tdata.list[i].querytype = querytypelist[data.list[i].querytype];\r\n\t\t\t\t}\r\n\t\t\t\tvmdata.colist = data.list;\r\n\t\t\t}\r\n\t\t\telse{\r\n\t\t\t\tvmdata.colist = [];\r\n\t\t\t}\r\n\t\t});\r\n\t}\r\n\r\n\tfunction getQuerytype(text){\r\n\t\tfor (var i = 0; i < querytypelist.length; i++){\r\n\t\t\tif (text == querytypelist[i]) return i;\r\n\t\t}\r\n\t\treturn 0;\r\n\t}\r\n\r\n\tfunction addColumn(index){\r\n\t\tvar data = Object.assign({}, vmdata.codata);\r\n\r\n\t\tdata[\'model\'] = {querytype: \'不用查询\', name: \'\', title: \'\', filter: \'\', remark: \'\'};\r\n\r\n\t\tvar elem = showToastDialog(data, \'添加字段\', function(flag){\r\n\t\t\tif (flag){\r\n\t\t\t\tvar column = \'\';\r\n\t\t\t\tvar colist = vmdata.colist;\r\n\r\n\t\t\t\tif (index < 0) column += \',\' + data.model.name;\r\n\r\n\t\t\t\tif (colist.length > 0){\r\n\t\t\t\t\tfor (var i = 0; i < colist.length; i++){\r\n\t\t\t\t\t\tcolumn += \',\' + colist[i].name;\r\n\t\t\t\t\t\tif (i == index) column += \',\' + data.model.name;\r\n\t\t\t\t\t}\r\n\t\t\t\t\tcolumn = column.substring(1);\r\n\t\t\t\t}\r\n\r\n\t\t\t\tvar param = Object.assign({flag: \'A\', column: column, tabid: \'";
	out<<(tabid);
	out<<"\'}, data.model);\r\n\r\n\t\t\t\tparam.querytype = getQuerytype(param.querytype);\r\n\r\n\t\t\t\tgetHttpResult(\'/columnview\', param, function(data){\r\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\r\n\t\t\t\t\t\tsessionTimeout();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\t\t\t\tshowNoAccessToast();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code < 0){\r\n\t\t\t\t\t\tshowToast(\'添加字段失败\');\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse{\r\n\t\t\t\t\t\tloadColumnList();\r\n\t\t\t\t\t\tshowToast(\'添加字段成功\');\r\n\t\t\t\t\t}\r\n\t\t\t\t});\r\n\t\t\t}\r\n\t\t});\r\n\r\n\t\tvar cx = $(elem.name).width();\r\n\t\t$(elem.filter).width(cx);\r\n\t\t$(elem.remark).width(cx);\r\n\t}\r\n\r\n\tfunction updateColumn(item){\r\n\t\tvar data = Object.assign({}, vmdata.codata);\r\n\r\n\t\tdata[\'model\'] = {querytype: item.querytype, name: item.name, title: item.title, filter: item.filter, remark: item.remark};\r\n\r\n\t\tvar elem = showToastDialog(data, \'添加字段\', function(flag){\r\n\t\t\tif (flag){\r\n\t\t\t\tvar param = Object.assign({flag: \'U\', tabid: \'";
	out<<(tabid);
	out<<"\'}, data.model);\r\n\r\n\t\t\t\tparam.querytype = getQuerytype(param.querytype);\r\n\r\n\t\t\t\tgetHttpResult(\'/columnview\', param, function(data){\r\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\r\n\t\t\t\t\t\tsessionTimeout();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\t\t\t\tshowNoAccessToast();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code < 0){\r\n\t\t\t\t\t\tshowToast(\'修改字段失败\');\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse{\r\n\t\t\t\t\t\tloadColumnList();\r\n\t\t\t\t\t\tshowToast(\'修改字段成功\');\r\n\t\t\t\t\t}\r\n\t\t\t\t});\r\n\t\t\t}\r\n\t\t});\r\n\r\n\t\tvar cx = $(elem.name).attr(\'disabled\', true).width();\r\n\t\t$(elem.filter).width(cx);\r\n\t\t$(elem.remark).width(cx);\r\n\t}\r\n\r\n\tfunction removeColumn(item){\r\n\t\tshowToastDialog(\"<div style=\'margin:20px\'>是否决定删除[\" + item.title + \']字段？</div>\', \'删除字段\', function(flag){\r\n\t\t\tif (flag){\r\n\t\t\t\tgetHttpResult(\'/columnview\', {flag: \'D\', name: item.name, tabid: \'";
	out<<(tabid);
	out<<"\'}, function(data){\r\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\r\n\t\t\t\t\t\tsessionTimeout();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\t\t\t\tshowNoAccessToast();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code < 0){\r\n\t\t\t\t\t\tshowToast(\'删除字段失败\');\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse{\r\n\t\t\t\t\t\tloadColumnList();\r\n\t\t\t\t\t\tshowToast(\'删除字段成功\');\r\n\t\t\t\t\t}\r\n\t\t\t\t});\r\n\t\t\t}\r\n\t\t});\r\n\t}\r\n\r\n\tloadColumnList();\r\n}\r\n</script>";


	return XG_OK;
}
HTTP_WEBAPP(ColumnView, CGI_PRIVATE, "ColumnView")