<%@ path=${filename}%>

<div></div>

<%
	param_string(flag);
	param_name_string(tabid);

	auto add = [&](){
	};

	auto remove = [&](){
	};

	auto update = [&](){
	};

	if (flag.length() > 0)
	{
		checkLogin();
		checkSystemRight();

		if (flag == "A")
		{
			add();
		}
		else if (flag == "D")
		{
			remove();
		}
		else if (flag == "U")
		{
			update();
		}

		return simpleResponse(XG_OK);
	}

	webx::PrintRecordview(out, tabid);
%>

<script>
{
	let vmdata = {
		title: ['数据ID', '数据源ID', '数据表名', '数据名称', '数据说明'],
		model: {id: '', dbid: '', name: '', title: '', remark: ''},
		style: [
			{size: 24, minlength: 1, maxlength: 32, filter: commonfilter.name},
			{size: 24, minlength: 1, maxlength: 32},
			{size: 24, minlength: 1, maxlength: 64, filter: commonfilter.name},
			{size: 24, minlength: 1, maxlength: 64},
			{size: 24, minlength: 0, maxlength: 256, type: 'textarea'}
		]
	};

	function remove(item){
		if (item.enabled < 2) return showToast('当前记录不可删除');

		showConfirmMessage('是否要删除数据[' + item.id + ']配置？', '删除选项', function(flag){
			if (flag){
				getHttpResult('/configview', {id: item.id, flag: 'D'}, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('删除记录失败');
					}
					else{
						$recordvmdata.recordview.reload(false);

						showToast('删除记录成功');
					}
				});
			}
		});
	}

	function update(item){
		if (item.enabled < 2) return showToast('当前记录不可修改');

		var data = Object.assign({}, vmdata);

		data.model = {id: item.id, dbid: item.dbid, name: item.name, title: item.title, remark: item.remark};

		var elem = showConfirmDialog(data, '修改配置', function(flag){
			if (flag){
				var param = Object.assign({flag: 'U'}, data.model);

				getHttpResult('/configview', param, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('修改配置失败');
					}
					else{
						$recordvmdata.recordview.reload(false);

						showToast('修改配置成功');
					}
				});
			}
		});

		$(elem.remark).width($.pack(elem.id).attr('disabled', true).width());
	}

	function columnView(item, elem){
		var msg = getHttpResult('/columnview', {tabid: item.id});

		showConfirmMessage(msg, '编辑字段', function(flag){
		}, null, true, false);

		$('#XG_MSGBOX_OPTION_ROW_ID').remove();
	}

	$('#AddRecordButton').css('color', '#090').click(function(){
		var data = Object.assign({}, vmdata);

		data.model = {id: '', dbid: '', name: '', title: '', remark: ''};

		var elem = showConfirmDialog(data, '添加配置', function(flag){
			if (flag){
				var param = Object.assign({flag: 'A'}, data.model);

				getHttpResult('/configview', param, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('添加配置失败');
					}
					else{
						$recordvmdata.recordview.reload(false);

						showToast('添加配置成功');
					}
				});
			}
		});

		$(elem.remark).width($.pack(elem.id).width());
	});

	$recordvmdata.button = [{
		title: '字段',
		color: '#090',
		click: columnView
	}, {
		title: '编辑',
		color: '#009',
		click: update
	}, {
		title: '删除',
		color: '#C00',
		click: remove
	}];
}
</script>