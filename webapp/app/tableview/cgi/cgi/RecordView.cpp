#include <webx/route.h>



class RecordView : public webx::ProcessBase
{
protected:
	int process();
};

int RecordView::process()
{
	out<<"<div></div>";
	param_string(flag);
	param_name_string(tabid);

	auto add = [&](){
	};

	auto remove = [&](){
	};

	auto update = [&](){
	};

	if (flag.length() > 0)
	{
		checkLogin();
		checkSystemRight();

		if (flag == "A")
		{
			add();
		}
		else if (flag == "D")
		{
			remove();
		}
		else if (flag == "U")
		{
			update();
		}

		return simpleResponse(XG_OK);
	}

	webx::PrintRecordview(out, tabid);
	out<<"<script>\r\n{\r\n\tlet vmdata = {\r\n\t\ttitle: [\'数据ID\', \'数据源ID\', \'数据表名\', \'数据名称\', \'数据说明\'],\r\n\t\tmodel: {id: \'\', dbid: \'\', name: \'\', title: \'\', remark: \'\'},\r\n\t\tstyle: [\r\n\t\t\t{size: 24, minlength: 1, maxlength: 32, filter: commonfilter.name},\r\n\t\t\t{size: 24, minlength: 1, maxlength: 32},\r\n\t\t\t{size: 24, minlength: 1, maxlength: 64, filter: commonfilter.name},\r\n\t\t\t{size: 24, minlength: 1, maxlength: 64},\r\n\t\t\t{size: 24, minlength: 0, maxlength: 256, type: \'textarea\'}\r\n\t\t]\r\n\t};\r\n\r\n\tfunction remove(item){\r\n\t\tif (item.enabled < 2) return showToast(\'当前记录不可删除\');\r\n\r\n\t\tshowConfirmMessage(\'是否要删除数据[\' + item.id + \']配置？\', \'删除选项\', function(flag){\r\n\t\t\tif (flag){\r\n\t\t\t\tgetHttpResult(\'/configview\', {id: item.id, flag: \'D\'}, function(data){\r\n\t\t\t\t\tif (data.code == XG_TIMEOUT){\r\n\t\t\t\t\t\tsessionTimeout();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\t\t\t\tshowNoAccessToast();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code < 0){\r\n\t\t\t\t\t\tshowToast(\'删除记录失败\');\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse{\r\n\t\t\t\t\t\t$recordvmdata.recordview.reload(false);\r\n\r\n\t\t\t\t\t\tshowToast(\'删除记录成功\');\r\n\t\t\t\t\t}\r\n\t\t\t\t});\r\n\t\t\t}\r\n\t\t});\r\n\t}\r\n\r\n\tfunction update(item){\r\n\t\tif (item.enabled < 2) return showToast(\'当前记录不可修改\');\r\n\r\n\t\tvar data = Object.assign({}, vmdata);\r\n\r\n\t\tdata.model = {id: item.id, dbid: item.dbid, name: item.name, title: item.title, remark: item.remark};\r\n\r\n\t\tvar elem = showConfirmDialog(data, \'修改配置\', function(flag){\r\n\t\t\tif (flag){\r\n\t\t\t\tvar param = Object.assign({flag: \'U\'}, data.model);\r\n\r\n\t\t\t\tgetHttpResult(\'/configview\', param, function(data){\r\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\r\n\t\t\t\t\t\tsessionTimeout();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\t\t\t\tshowNoAccessToast();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code < 0){\r\n\t\t\t\t\t\tshowToast(\'修改配置失败\');\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse{\r\n\t\t\t\t\t\t$recordvmdata.recordview.reload(false);\r\n\r\n\t\t\t\t\t\tshowToast(\'修改配置成功\');\r\n\t\t\t\t\t}\r\n\t\t\t\t});\r\n\t\t\t}\r\n\t\t});\r\n\r\n\t\t$(elem.remark).width($.pack(elem.id).attr(\'disabled\', true).width());\r\n\t}\r\n\r\n\tfunction columnView(item, elem){\r\n\t\tvar msg = getHttpResult(\'/columnview\', {tabid: item.id});\r\n\r\n\t\tshowConfirmMessage(msg, \'编辑字段\', function(flag){\r\n\t\t}, null, true, false);\r\n\r\n\t\t$(\'#XG_MSGBOX_OPTION_ROW_ID\').remove();\r\n\t}\r\n\r\n\t$(\'#AddRecordButton\').css(\'color\', \'#090\').click(function(){\r\n\t\tvar data = Object.assign({}, vmdata);\r\n\r\n\t\tdata.model = {id: \'\', dbid: \'\', name: \'\', title: \'\', remark: \'\'};\r\n\r\n\t\tvar elem = showConfirmDialog(data, \'添加配置\', function(flag){\r\n\t\t\tif (flag){\r\n\t\t\t\tvar param = Object.assign({flag: \'A\'}, data.model);\r\n\r\n\t\t\t\tgetHttpResult(\'/configview\', param, function(data){\r\n\t\t\t\t\tif (data.code ==  XG_TIMEOUT){\r\n\t\t\t\t\t\tsessionTimeout();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code == XG_AUTHFAIL){\r\n\t\t\t\t\t\tshowNoAccessToast();\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse if (data.code < 0){\r\n\t\t\t\t\t\tshowToast(\'添加配置失败\');\r\n\t\t\t\t\t}\r\n\t\t\t\t\telse{\r\n\t\t\t\t\t\t$recordvmdata.recordview.reload(false);\r\n\r\n\t\t\t\t\t\tshowToast(\'添加配置成功\');\r\n\t\t\t\t\t}\r\n\t\t\t\t});\r\n\t\t\t}\r\n\t\t});\r\n\r\n\t\t$(elem.remark).width($.pack(elem.id).width());\r\n\t});\r\n\r\n\t$recordvmdata.button = [{\r\n\t\ttitle: \'字段\',\r\n\t\tcolor: \'#090\',\r\n\t\tclick: columnView\r\n\t}, {\r\n\t\ttitle: \'编辑\',\r\n\t\tcolor: \'#009\',\r\n\t\tclick: update\r\n\t}, {\r\n\t\ttitle: \'删除\',\r\n\t\tcolor: \'#C00\',\r\n\t\tclick: remove\r\n\t}];\r\n}\r\n</script>";


	return XG_OK;
}
HTTP_WEBAPP(RecordView, CGI_PRIVATE, "RecordView")