<%@ path=${filename}%>

<%
	param_string(flag);
	param_string(name);
	param_string(title);
	param_string(tabid);
	param_string(filter);
	param_string(remark);
	param_string(querytype);

	checkLogin();

	auto add = [&](){
		param_string(column);

		auto vec = stdx::split(column, ",");
		auto dbconn = webx::GetDBConnect();
		
		for (int i = 0; i < vec.size(); i++)
		{
			const string& field = vec[i];

			if (name == field)
			{
				dbconn->execute("INSERT INTO T_XG_TABCOLS(NAME,TABID,TITLE,FILTER,REMARK,QUERYTYPE,POSITION,ENABLED,STATETIME) VALUES(?,?,?,?,?,?,?,2,?)", name, tabid, title, filter, remark, querytype, i, DateTime::ToString());
			}
			else
			{
				dbconn->execute("UPDATE T_XG_TABCOLS SET position=? WHERE TABID=? AND NAME=? AND ENABLED>1", i, tabid, field);
			}
		}

	
		return XG_OK;
	};

	auto remove = [&](){
		return webx::GetDBConnect()->execute("DELETE FROM T_XG_TABCOLS WHERE TABID=? AND NAME=? AND ENABLED>1", tabid, name);
	};

	auto update = [&](){
		return webx::GetDBConnect()->execute("UPDATE T_XG_TABCOLS SET TITLE=?,REMARK=?,FILTER=?,QUERYTYPE=?,STATETIME=? WHERE TABID=? AND NAME=? AND ENABLED>1", title, remark, filter, querytype, DateTime::ToString(), tabid, name);
	};

	if (flag.length() > 0)
	{
		int res = XG_OK;

		if (flag == "A")
		{
			res = add();
		}
		else if (flag == "D")
		{
			res = remove();
		}
		else if (flag == "U")
		{
			res = update();
		}

		return simpleResponse(res);
	}
%>

<style>
#RecordColumnDiv{
	min-height: 60vh;
	background: #CCC;
}
#RecordColumnDiv table{
	min-width: 60vw;
}
#RecordColumnDiv table td{
	padding: 5px 4px;
}
#RecordColumnDiv table button{
	color: #090;
	margin: 0px 3px;
	padding: 1px 2px;
}
#RecordColumnDiv table tr:first-child td{
	font-size: 0.95rem;
	font-weight: bold;
}
#RecordColumnDiv table tr:nth-child(even){
	background: rgba(180, 180, 180, 0.5);
}
#RecordColumnDiv table tr:nth-child(odd){
	background:rgba(100, 160, 200, 0.5);
}
#RecordColumnDiv table tr:first-child{
	background: rgba(80, 0, 80, 0.5);
}
</style>

<div id='RecordColumnDiv'>
	<table>
		<tr>
			<td>字段名</td>
			<td>字段标题</td>
			<td>查询类型</td>
			<td>过滤条件</td>
			<td>字段说明</td>
			<td><button @click='addColumn(-1)' class='TextButton'>添加</button></td>
		</tr>
		<tr v-for='(item,index) in colist'>
			<td>{{item.name}}</td>
			<td>{{item.title}}</td>
			<td>{{item.querytype}}</td>
			<td>{{item.filter}}</td>
			<td>{{item.remark}}</td>
			<td>
				<button @click='addColumn(index)' class='TextButton'>添加</button>
				<button @click='updateColumn(item)' class='TextButton' style='color:#009'>编辑</button>
				<button @click='removeColumn(item)' class='TextButton' style='color:#C00'>删除</button>
			</td>
		</tr>
	</table>
</div>

<script>
{
	let querytypelist = ['不用查询', '完全匹配', '前缀匹配', '模糊匹配', '日期范围', '时间范围'];

	let vmdata = {
		colist: [],
		codata: {
			title: ['查询条件', '字段名', '字段标题', '过滤条件', '字段说明'],
			model: {querytype: '不用查询', name: '', title: '', filter: '', remark: ''},
			style: [
				{select: querytypelist},
				{size: 24, minlength: 1, maxlength: 32, filter: commonfilter.name},
				{size: 24, minlength: 1, maxlength: 32},
				{size: 24, minlength: 0, maxlength: 256, type: 'textarea'},
				{size: 24, minlength: 0, maxlength: 256, type: 'textarea'}
			]
		}
	}

	getVue('RecordColumnDiv', vmdata);

	function loadColumnList(){
		getHttpResult('/getcolumnlist', {tabid: '<%=tabid%>'}, function(data){
			if (data.code > 0){
				for (var i = 0; i < data.list.length; i++){
					data.list[i].querytype = querytypelist[data.list[i].querytype];
				}
				vmdata.colist = data.list;
			}
			else{
				vmdata.colist = [];
			}
		});
	}

	function getQuerytype(text){
		for (var i = 0; i < querytypelist.length; i++){
			if (text == querytypelist[i]) return i;
		}
		return 0;
	}

	function addColumn(index){
		var data = Object.assign({}, vmdata.codata);

		data['model'] = {querytype: '不用查询', name: '', title: '', filter: '', remark: ''};

		var elem = showToastDialog(data, '添加字段', function(flag){
			if (flag){
				var column = '';
				var colist = vmdata.colist;

				if (index < 0) column += ',' + data.model.name;

				if (colist.length > 0){
					for (var i = 0; i < colist.length; i++){
						column += ',' + colist[i].name;
						if (i == index) column += ',' + data.model.name;
					}
					column = column.substring(1);
				}

				var param = Object.assign({flag: 'A', column: column, tabid: '<%=tabid%>'}, data.model);

				param.querytype = getQuerytype(param.querytype);

				getHttpResult('/columnview', param, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('添加字段失败');
					}
					else{
						loadColumnList();
						showToast('添加字段成功');
					}
				});
			}
		});

		var cx = $(elem.name).width();
		$(elem.filter).width(cx);
		$(elem.remark).width(cx);
	}

	function updateColumn(item){
		var data = Object.assign({}, vmdata.codata);

		data['model'] = {querytype: item.querytype, name: item.name, title: item.title, filter: item.filter, remark: item.remark};

		var elem = showToastDialog(data, '添加字段', function(flag){
			if (flag){
				var param = Object.assign({flag: 'U', tabid: '<%=tabid%>'}, data.model);

				param.querytype = getQuerytype(param.querytype);

				getHttpResult('/columnview', param, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('修改字段失败');
					}
					else{
						loadColumnList();
						showToast('修改字段成功');
					}
				});
			}
		});

		var cx = $(elem.name).attr('disabled', true).width();
		$(elem.filter).width(cx);
		$(elem.remark).width(cx);
	}

	function removeColumn(item){
		showToastDialog("<div style='margin:20px'>是否决定删除[" + item.title + ']字段？</div>', '删除字段', function(flag){
			if (flag){
				getHttpResult('/columnview', {flag: 'D', name: item.name, tabid: '<%=tabid%>'}, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('删除字段失败');
					}
					else{
						loadColumnList();
						showToast('删除字段成功');
					}
				});
			}
		});
	}

	loadColumnList();
}
</script>