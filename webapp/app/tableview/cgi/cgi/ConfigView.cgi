<%@ path=${filename}%>

<div><green v-show='configviewaccess>0' @click='addRecordItem()' class='TextButton'>添加表单</green></div>

<%
	param_string(id);
	param_string(flag);
	param_string(name);
	param_string(dbid);
	param_string(title);
	param_string(remark);

	auto add = [&](){
		return webx::GetDBConnect()->execute("INSERT INTO T_XG_TABETC(ID,DBID,NAME,TITLE,REMARK,ENABLED,STATETIME) VALUES(?,?,?,?,?,2,?)", id, dbid, name, title, remark, DateTime::ToString());
	};

	auto remove = [&](){
		int res = webx::GetDBConnect()->execute("DELETE FROM T_XG_TABETC WHERE ID=? AND ENABLED>1", id);

		if (res > 0) webx::GetDBConnect()->execute("DELETE FROM T_XG_TABCOLS WHERE TABID=?", id);

		return res;
	};

	auto update = [&](){
		return webx::GetDBConnect()->execute("UPDATE T_XG_TABETC SET DBID=?,NAME=?,TITLE=?,REMARK=?,STATETIME=? WHERE ID=? AND ENABLED>1", dbid, name, title, remark, DateTime::ToString(), id);
	};

	if (flag.length() > 0)
	{
		checkLogin();

		int res = XG_OK;

		if (flag == "A")
		{
			res = add();
		}
		else if (flag == "D")
		{
			res = remove();
		}
		else if (flag == "U")
		{
			res = update();
		}

		return simpleResponse(res);
	}

	webx::PrintRecordview(out, "${config}");
%>

<script>
{
	var configviewaccess = getAccess('/configview');

	$recordvmdata.button = [{
			title: '字段',
			click: columnView,
			disable: configviewaccess < 0
		}, {
			title: '编辑',
			click: update,
			disable: configviewaccess < 0
		}, {
			title: '删除',
			click: remove,
			disable: configviewaccess < 0
	}];

	var vmdata = {
		title: ['数据ID', '数据源ID', '数据表名', '数据名称', '数据说明'],
		model: {id: '', dbid: '', name: '', title: '', remark: ''},
		style: [
			{size: 24, minlength: 1, maxlength: 32, filter: commonfilter.name},
			{size: 24, minlength: 1, maxlength: 32},
			{size: 24, minlength: 1, maxlength: 64, filter: commonfilter.name},
			{size: 24, minlength: 1, maxlength: 64},
			{size: 24, minlength: 0, maxlength: 256, type: 'textarea'}
		]
	};

	function addRecordItem(){
		var data = Object.assign({}, vmdata);

		data.model = {id: '', dbid: '', name: '', title: '', remark: ''};

		var elem = showConfirmDialog(data, '添加配置', function(flag){
			if (flag){
				var param = Object.assign({flag: 'A'}, data.model);

				getHttpResult('/configview', param, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('添加配置失败');
					}
					else{
						$recordvmdata.recordview.reload(false);
						showToast('添加配置成功');
					}
				});
			}
		});

		$(elem.remark).width($.pack(elem.id).width());
	}
	
	function remove(item){
		if (item.enabled < 2) return showToast('当前记录不可删除');

		showConfirmMessage('是否要删除数据[' + item.id + ']配置？', '删除选项', function(flag){
			if (flag){
				getHttpResult('/configview', {id: item.id, flag: 'D'}, function(data){
					if (data.code == XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('删除记录失败');
					}
					else{
						$recordvmdata.recordview.reload(false);
						showToast('删除记录成功');
					}
				});
			}
		});
	}

	function update(item){
		if (item.enabled < 2) return showToast('当前记录不可修改');

		var data = Object.assign({}, vmdata);

		data.model = {id: item.id, dbid: item.dbid, name: item.name, title: item.title, remark: item.remark};

		var elem = showConfirmDialog(data, '修改配置', function(flag){
			if (flag){
				var param = Object.assign({flag: 'U'}, data.model);

				getHttpResult('configview', param, function(data){
					if (data.code ==  XG_TIMEOUT){
						sessionTimeout();
					}
					else if (data.code == XG_AUTHFAIL){
						showNoAccessToast();
					}
					else if (data.code < 0){
						showToast('修改配置失败');
					}
					else{
						$recordvmdata.recordview.reload(false);
						showToast('修改配置成功');
					}
				});
			}
		});

		$(elem.remark).width($.pack(elem.id).attr('disabled', true).width());
	}

	function columnView(item, elem){
		var msg = getHttpResult('/columnview', {tabid: item.id});

		showConfirmMessage(msg, '编辑字段', function(flag){
		}, null, true, false);

		$('#XG_MSGBOX_OPTION_ROW_ID').remove();
	}
}
</script>