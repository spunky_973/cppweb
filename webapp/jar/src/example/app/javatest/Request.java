package example.app.javatest;

import stdx.Optional;
import stdx.Required;

public class Request{
	@Required(value = "用户名", regex="[0-9|a-z|A-Z]{4,32}")
	public String user;

	@Optional(value = "用户昵称", length = {1, 32})
	public String name;
}