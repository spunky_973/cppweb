#include <webx/route.h>
#include <http/HttpHelper.h>
#include <dbentity/T_XG_USER.h>
#include <dbentity/T_XG_CODE.h>
#include <dbentity/T_XG_NOTE.h>
#include <dbentity/T_XG_PARAM.h>
#include <dbentity/T_XG_DBETC.h>
#include <dbentity/T_XG_TIMER.h>
#include <dbentity/T_XG_GROUP.h>

typedef webx::AccessItem AccessItem;

static thread_local int remotestatus = 0;
static HttpServer* app = HttpServer::Instance();

class TimerTask : public WorkItem
{
public:
	int delay;
	int status;
	string path;
	time_t rtime;
	time_t utime;
	string timeval;
	atomic_bool skip;

	bool uptime()
	{
		if (timeval.find(':') == string::npos)
		{
			rtime = utime;
		}
		else
		{
			rtime = DateTime::FromString(DateTime(utime).getDateString() + " " + timeval).getTime();
		}

		return true;
	}
	void process()
	{
		skip = stdx::async([this]{
			SmartBuffer data = webx::GetRemoteResult(this->path);

			if (data.str())
			{
				int status = 0;
				string code = JsonElement(data.str()).asString("code");

				if (code.empty())
				{
					int code = webx::GetLastRemoteStatus();

					if (code == 200)
					{
						status = XG_OK;
					}
					else if (code == 404)
					{
						status = XG_UNINSTALL;
					}
					else
					{
						status = XG_ERROR;
					}
				}
				else
				{
					status = stdx::atoi(code.c_str());

					status = status >= 0 ? XG_OK : XG_ERROR;
				}

				if (status)
				{
					this->utime = time(NULL);
					this->save(status);
				}
			}

			this->skip = false;
		});
	}
	bool runnable()
	{
		if (skip) return false;

		if (status == XG_UNINSTALL) return true;

		string val;
		time_t now = time(NULL);

		Check(path, false, val);

		if (val.empty())
		{
			save(XG_UNINSTALL);

			return true;
		}

		if (val == timeval)
		{
			if (rtime + delay > now) return false;

			utime = now;

			save();
			uptime();
			process();
		}
		else
		{
			if (update(val))
			{
				LogTrace(eINF, "update timertask[%s][%s] success", path.c_str(), val.c_str());
			}
			else
			{
				LogTrace(eERR, "update timertask[%s][%s] failed", path.c_str(), val.c_str());
			}
		}

		return false;
	}
	bool save(int status = 0)
	{
		CT_XG_TIMER tab;

		try
		{
			tab.init(webx::GetDBConnect());
		}
		catch(Exception e)
		{
			return false;
		}

		tab.path = path;
		tab.status = status;
		tab.timeval = timeval;
		tab.statetime = DateTime(utime);

		if ((this->status = status) == XG_UNINSTALL) return Check(path, true) && tab.remove() > 0;

		if (status = tab.update()) return status > 0;

		return tab.insert() > 0;
	}
	bool update(const string& timeval)
	{
		CT_XG_TIMER tab;

		try
		{
			tab.init(webx::GetDBConnect());
		}
		catch(Exception e)
		{
			return false;
		}

		tab.path = path;

		int delay = timeval.find(':') == string::npos ? stdx::atoi(timeval.c_str()) : 24 * 3600;

		if (tab.find() && tab.next())
		{
			utime = tab.statetime.val().getTime();

			tab.timeval = timeval;
			
			if (tab.update() < 0) return false;

			if (timeval.find(':') != string::npos)
			{
				string now = tab.statetime.toString();
				string date = DateTime(time(NULL)).getDateString();
	
				if (now.find(date) == 0 && timeval > now.substr(11)) utime -= delay;
			}
		}
		else
		{
			utime = time(NULL) - delay;

			tab.statetime = DateTime(utime);
			tab.timeval = timeval;
			tab.status = 0;

			if (tab.insert() < 0) return false;
		}

		this->timeval = timeval;
		this->delay = delay;

		return uptime();
	}
	bool init(const string& path, const string& timeval)
	{
		this->path = path;
		this->skip = false;

		return update(timeval);
	}

	static void Clear(sp<DBConnect> dbconn)
	{
		dbconn->execute("DELETE FROM T_XG_TIMER WHERE STATETIME<?", DateTime(time(NULL) - 3 * 24 * 3600));
	}
	static bool Check(const string& path, bool clear)
	{
		string timeval;

		return Check(path, clear, timeval);
	}
	static bool Check(const string& path, bool clear, string& timeval)
	{
		static map<string, string> pathmap;
		static SpinMutex mtx;
		SpinLocker lk(mtx);

		if (path.front() == '{' && path.back() == '}')
		{
			vector<string> vec;

			for (const auto& item : pathmap)
			{
				if (path.find("{" + item.first + "}") == string::npos)
				{
					vec.push_back(item.first);
				}
			}

			if (vec.size() > 0)
			{
				CT_XG_TIMER tab;

				try
				{
					tab.init(webx::GetDBConnect());
				}
				catch(Exception e)
				{
					return false;
				}

				for (const string& path : vec)
				{
					pathmap.erase(path);
					tab.path = path;
					tab.remove();
				}
			}

			return true;
		}

		if (clear)
		{
			pathmap.erase(path);

			return true;
		}

		string& val = pathmap[path];

		if (val.empty())
		{
			val = timeval;

			return true;
		}

		if (timeval.empty())
		{
			timeval = val;
		}
		else
		{
			val = timeval;
		}

		return false;
	}
};

class RouteConfig
{
protected:
	int port;
	bool route;
	string host;
	string cgiversion;
	string accessversion;
	mutable SpinMutex mtx;
	map<string, vector<HostItem>> hostmap;
	map<string, vector<AccessItem>> accessmap;

public:
	void init()
	{
		ConfigFile* cfg = app->getConfigFile();

		cfg->getVariable("ROUTE_HOST", host);
		cfg->getVariable("ROUTE_PORT", port);

		route = port > 0 && host.length() > 0;
	}
	RouteConfig()
	{
		route = false;
		port = 0;
	}
	HostItem getHost() const
	{
		SpinLocker lk(mtx);

		return HostItem(host, port);
	}
	HostItem get(const string& path) const
	{
		HostItem item;
		SpinLocker lk(mtx);
		const auto it = hostmap.find(CgiMapData::GetKey(path));

		if (it == hostmap.end()) return item;

		const vector<HostItem>& vec = it->second;

		if (vec.empty()) return item;

		return vec[abs(rand()) % vec.size()];
	}
	vector<HostItem> getList(const string& path) const
	{
		SpinLocker lk(mtx);
		const auto it = hostmap.find(CgiMapData::GetKey(path));

		if (it == hostmap.end()) return vector<HostItem>();

		return it->second;
	}
	bool updateRoute(const string& host, int port)
	{
		CHECK_FALSE_RETURN(host.length() > 0 && port > 0);

		JsonElement json;
		HttpRequest request("exportroute");

		request.setParameter("version", cgiversion);
		request.setParameter("access", CGI_PROTECT);

		if (route)
		{
			request.setParameter("clientid", app->getId());
			request.setParameter("clientname", app->getName());
			request.setParameter("clientport", app->getPort());
			request.setParameter("clienthost", app->getHost());
		}

		SmartBuffer buffer = request.getResult(host, port);

		CHECK_FALSE_RETURN(buffer.str() && json.init(buffer.str()));

		string version = json.asString("version");

		if (version == cgiversion)
		{
			SpinLocker lk(mtx);

			this->host = host;
			this->port = port;

			return true;
		}

		json = json.get("list");

		CHECK_FALSE_RETURN(json.isArray());

		string pathlist;
		map<string, vector<HostItem>> hostmap;

		for (JsonElement item : json)
		{
			int weight;
			HostItem host;
			JsonElement hostlist = item["list"];
			string path = item["path"].asString();
			vector<HostItem>& vec = hostmap[path];

			pathlist += "{" + path + "}";

			for (JsonElement data : hostlist)
			{
				weight = data["weight"].asInt();
				host.port = data["port"].asInt();
				host.host = data["host"].asString();

				if (weight < 1) weight = 1;
				if (weight > 9) weight = 9;

				for (int i = 0; i < weight; i++) vec.push_back(host);
			}
		}

		if (pathlist.length() > 0) TimerTask::Check(pathlist, false, pathlist);

		mtx.lock();

		std::swap(this->hostmap, hostmap);
		this->cgiversion = version;
		this->host = host;
		this->port = port;

		mtx.unlock();
		
		return true;
	}
	bool updateAccess(const HostItem& route)
	{
		string version;
		map<string, vector<AccessItem>> accessmap;

		if (route.canUse())
		{
			JsonElement json;
			HttpRequest request("exportaccess");

			request.setParameter("version", accessversion);

			SmartBuffer buffer = request.getResult(route.host, route.port);

			CHECK_FALSE_RETURN(buffer.str() && json.init(buffer.str()));

			version = json.asString("version");

			if (version == accessversion)
			{
				version.clear();
			}
			else
			{
				json = json.get("list");

				CHECK_FALSE_RETURN(json.isArray());

				for (JsonElement item : json)
				{
					string path = item["path"].asString();
					string param = item["param"].asString();
					string grouplist = item["grouplist"].asString();

					accessmap[path].push_back(AccessItem(param, stdx::split(grouplist, ",")));
				}
			}
		}

		CHECK_FALSE_RETURN(webx::LoadAccessMap(accessmap));

		mtx.lock();

		if (version.length() > 0) accessversion = version;

		std::swap(this->accessmap, accessmap);

		mtx.unlock();

		return true;
	}
	int checkAccess(const string& path, const string& param, const string& grouplist)
	{
		vector<string> vec = stdx::split(grouplist, ",");

		for (const string& group : vec)
		{
			if (group == "root") return XG_OK;
		}

		{
			int res = 0;
			SpinLocker lk(mtx);
			auto it = accessmap.find(CgiMapData::GetKey(path));
			
			if (it == accessmap.end()) return XG_FAIL;

			for (const AccessItem& item : it->second)
			{
				if (res = item.check(param, vec)) return res;
			}

			return res;
		}
	}

	static RouteConfig* Instance()
	{
		static RouteConfig route;

		return &route;
	}
};

class NotifyItem : public WorkItem
{
public:
	int port;
	string host;
	string path;
	string param;
	string cookie;
	string contype;

public:
	void run()
	{
		HttpRequest request(path);

		request.setDataString(param);

		if (cookie.length() > 0) request.setCookie(cookie);
		if (contype.length() > 0) request.setContentType(contype);

		if (request.getResponse(host, port))
		{
			LogTrace(eINF, "notify[%s:%d][%s] success", host.c_str(), port, path.c_str());
		}
		else
		{
			LogTrace(eERR, "notify[%s:%d][%s] failed", host.c_str(), port, path.c_str());
		}
	}
};

class PingWorkItem : public WorkItem
{
public:
	int port;
	int errcnt;
	int maxcnt;
	string host;

public:
	PingWorkItem()
	{
		errcnt = 0;
		maxcnt = 3;
	}
	bool runnable()
	{
		if (process() >= 0) return true;

		++errcnt;

		return false;
	}
	int process()
	{
		HttpRequest request("getcgilist");

		request.setParameter("access", CGI_PROTECT);
		request.setParameter("routehost", app->getHost());
		request.setParameter("routeport", app->getPort());

		HostItem loghost = webx::GetLogHost();

		if (loghost.canUse())
		{
			request.setParameter("loghost", loghost.host);
			request.setParameter("logport", loghost.port);
		}
		else
		{
			request.setParameter("loghost", app->getHost());
			request.setParameter("logport", app->getPort());
		}

		if (RedisConnect::CanUse())
		{
			sp<RedisConnect> redis = RedisConnect::Instance();

			if (redis)
			{
				request.setParameter("redishost", redis->getHost());
				request.setParameter("redisport", redis->getPort());
				request.setParameter("redispasswd", redis->getPassword());
			}
		}

		long us;
		Timer tr(NULL);
		string content;
		JsonElement json;
		SmartBuffer buffer = request.getResult(host, port);

		us = tr.getTimeGap();

		if (buffer.str() && json.init(buffer.str()) && json["list"].isArray())
		{
			LogTrace(eINF, "ping host[%s:%d] success[%ld]", host.c_str(), port, us);

			content = buffer.str();
		}
		else
		{
			LogTrace(eERR, "ping host[%s:%d] failed", host.c_str(), port);

			if (errcnt < maxcnt) return XG_NETERR;
		}

		while (maxcnt-- > 0)
		{
			string sqlcmd;
			sp<DBConnect> dbconn;

			try
			{
				dbconn = webx::GetDBConnect();
			}
			catch(Exception e)
			{
				Sleep(100);

				continue;
			}

			if (content.empty())
			{
				sqlcmd = "UPDATE T_XG_ROUTE SET PROCTIME=0 WHERE HOST=? AND PORT=?";

				return dbconn->execute(sqlcmd, host, port) < 0 ? XG_FAIL : XG_OK;
			}
			else
			{
				JsonElement item;
				JsonElement json(content);
				JsonElement list = json.get("list");

				for (int i = list.size() - 1; i >= 0; i--)
				{
					item = list.get(i);

					string extdata = item.asString("extdata");

					if (extdata.length() > 0)
					{
						HttpDataNode param;

						param.parse(extdata);

						string path = item.asString("path");
						string delay = param.getValue("timertask");
						string daily = param.getValue("dailytask");

						auto checkTimer = [](const string& path, const string& timeval){
							string val = timeval;

							if (TimerTask::Check(path, false, val))
							{
								sp<TimerTask> timer = newsp<TimerTask>();

								if (timer->init(path, val) && stdx::async(timer))
								{
									LogTrace(eINF, "create timertask[%s][%s] success", path.c_str(), val.c_str());
								}
								else
								{
									LogTrace(eINF, "create timertask[%s][%s] failed", path.c_str(), val.c_str());

									TimerTask::Check(path, true);
								}
							}
						};

						if (delay.length() > 0)
						{
							int val = stdx::atoi(delay.c_str());

							if (val > 0)
							{
								checkTimer(path, stdx::str(val));
							}
							else
							{
								LogTrace(eINF, "invalid timertask[%s][%s]", path.c_str(), delay.c_str());
							}
						}

						if (daily.length() > 0)
						{
							DateTime dt = DateTime::FromString(DateTime::ToString().substr(0, 11) + daily);

							if (dt.canUse())
							{
								checkTimer(path, dt.getTimeString());
							}
							else
							{
								LogTrace(eINF, "invalid dailytask[%s][%s]", path.c_str(), daily.c_str());
							}
						}
					}
				}

				sqlcmd = "UPDATE T_XG_ROUTE SET PROCTIME=?,CONTENT=?,STATETIME=? WHERE HOST=? AND PORT=?";

				return dbconn->execute(sqlcmd, us, content, DateTime().update(), host, port) < 0 ? XG_FAIL : XG_OK;
			}
		}

		return XG_FAIL;
	}
};

class PingWorkThread : public Thread
{
	atomic_long utime;

public:
	void run()
	{
		int index = 0;
		string sqlcmd = "SELECT HOST,PORT FROM T_XG_ROUTE WHERE ENABLED>0 GROUP BY HOST,PORT";

		utime = time(NULL);

		while (app->isActive())
		{
			if (app->getRouteSwitch())
			{
				sp<RowData> row;
				sp<QueryResult> rs;
				sp<DBConnect> dbconn;
				vector<sp<WorkItem>> vec;

				try
				{
					dbconn = webx::GetDBConnect();
				}
				catch(Exception e)
				{
					Sleep(100);

					continue;
				}

				if (rs = dbconn->query(sqlcmd))
				{
					LogTrace(eINF, "start route ping process success");

					while (row = rs->next())
					{
						sp<PingWorkItem> item = newsp<PingWorkItem>();
						
						item->host = row->getString(0);
						item->port = row->getInt(1);
						vec.push_back(item);
					}
				}

				if (index % 100 == 0) TimerTask::Clear(dbconn);

				dbconn = NULL;

				for (auto item : vec)
				{
					stdx::async(item);

					Sleep(100);
				}

				sp<Session> session = webx::GetLocaleSession("SYSTEM_ROUTELIST");

				if (session) session->clear();
			}

			updateRouteList();
			updateAcccessList();

			Sleep(5000);

			++index;
		}
	}
	void updateRouteList()
	{
		HostItem item = RouteConfig::Instance()->getHost();

		if (item.canUse())
		{
			if (RouteConfig::Instance()->updateRoute(item.host, item.port))
			{
				LogTrace(eINF, "update route list success");

				utime = time(NULL);
			}
			else
			{
				LogTrace(eERR, "update route list failed");
			}
		}
	}
	void updateAcccessList()
	{
		HostItem item = RouteConfig::Instance()->getHost();

		if (RouteConfig::Instance()->updateAccess(item))
		{
			LogTrace(eINF, "update access list success");
		}
		else
		{
			LogTrace(eERR, "update access list failed");
		}
	}
	time_t getUpdateTime() const
	{
		return utime;
	}

	static PingWorkThread* Instance()
	{
		static PingWorkThread thread;

		return &thread;
	}
};

static BOOL NeedUpdateRouteHost()
{
	return RouteConfig::Instance()->getHost().canUse() ? PingWorkThread::Instance()->getUpdateTime() + 30 < time(NULL) : true;
}

static BOOL InitNewUser(const char* user)
{
	try
	{
		CT_XG_USER tmp;

		tmp.init(webx::GetDBConnect());

		tmp.user = user;

		CHECK_FALSE_RETURN(tmp.find() && tmp.next());

		string dbid = tmp.dbid.val();

		CHECK_FALSE_RETURN(tmp.find("USER='template'") && tmp.next());

		int res;
		CT_XG_NOTE note;
		CT_XG_CODE code;
		CT_XG_PARAM param;
		vector<CT_XG_NOTE> notevec;
		vector<CT_XG_CODE> codevec;
		vector<CT_XG_PARAM> paramvec;
		sp<DBConnect> srconn = tmp.dbid.val().empty() ? tmp.getHandle() : webx::GetDBConnect(tmp.dbid.val());

		if (note.init(srconn) && note.find("USER='template'"))
		{
			while (note.next())
			{
				notevec.push_back(note);
				notevec.back().user = user;
			}
		}
		
		if (code.init(srconn) && code.find("USER='template'"))
		{
			while (code.next())
			{
				codevec.push_back(code);
				codevec.back().user = user;
			}
		}

		if (param.init(srconn) && param.find("ID LIKE 'template.%'"))
		{
			while (param.next())
			{
				paramvec.push_back(param);
				paramvec.back().id = user + param.id.val().substr(8);
			}
		}

		sp<DBConnect> destconn = dbid == tmp.dbid.val() ? srconn : webx::GetDBConnect(dbid);

		for (auto& tab : notevec)
		{
			tab.init(destconn, false);

			for (int i = 0; i < 5; i++)
			{
				tab.id = DateTime::GetBizId();
				if ((res = tab.insert()) >= 0) break;
			}
		}

		for (auto& tab : codevec)
		{
			tab.init(destconn, false);

			for (int i = 0; i < 5; i++)
			{
				tab.id = DateTime::GetBizId();
				if ((res = tab.insert()) >= 0) break;
			}
		}

		for (auto& tab : paramvec)
		{
			tab.init(destconn, false);
			tab.insert();
		}

		return true;
	}
	catch (Exception e)
	{
		return false;
	}
};

static void AsyncInitNewUser(const char* user)
{
	string newuser = user;

	stdx::async([=](){
		try{
			InitNewUser(newuser.c_str());
		}
		catch(Exception e)
		{
			LogTrace(eERR, "initialize user[%s] failed[%d][%s]", newuser.c_str(), e.getErrorCode(), e.getErrorString());
		}
	});
}

static int GetString(const string& val, char* dest, int size)
{
	if (--size > val.length()) size = val.length();

	if (size > 0)
	{
		strcpy(dest, val.c_str());
	}
	else
	{
		*dest = 0;
	}

	return val.length();
};

static int GetParameter(const char* id, char* dest, int size)
{
	sp<Session> session = webx::GetLocaleSession("SYSTEM_PARAMETER", 600);

	if (!session) return XG_SYSERR;

	string val = session->get(id);

	if (val.length() > 0) return GetString(val, dest, size);

	string sqlcmd = "SELECT PARAM FROM T_XG_PARAM WHERE ID=?";

	try
	{
		if (webx::GetDBConnect()->select(val, sqlcmd, id) < 0) return XG_SYSERR;

		session->set(id, val = stdx::translate(val));

		return GetString(val, dest, size);
	}
	catch(Exception e)
	{
		return XG_SYSBUSY;
	}
}

static DBConnectPool* GetDBConnectPool(const char* id)
{
	static SpinMutex mtx;
	static map<string, DBConnectPool*> dbmap;

	char buffer[1024];
	SpinLocker lk(mtx);
	auto it = dbmap.find(id);

	if (it != dbmap.end()) return it->second;

	ConfigFile cfg;
	CT_XG_DBETC tab;
	sp<DBConnect> dbconn;

	try
	{
		dbconn = webx::GetDBConnect();
	}
	catch(Exception e)
	{
		return NULL;
	}

	tab.init(dbconn);
	tab.id = id;

	if (!tab.find())
	{
		LogTrace(eERR, "query database pool[%s] failed", id);

		return NULL;
	}

	if (!tab.next())
	{
		LogTrace(eERR, "database pool[%s] undefined", id);

		return NULL;
	}

	if (tab.enabled.val() <= 0)
	{
		LogTrace(eERR, "database pool[%s] disable", id);

		return NULL;
	}

	string key = "DLLPATH_" + stdx::toupper(tab.type.val());

	if (GetParameter(key.c_str(), buffer, sizeof(buffer)) > 0)
	{
		cfg.setVariable("DLLPATH", buffer);
	}
	else
	{
		string path = stdx::tolower(tab.type.val());

		path = stdx::translate("$PRODUCT_HOME/dll/libdbx." + path + "pool.so");

		cfg.setVariable("DLLPATH", path);
	}
	
	cfg.setVariable("HOST", tab.host.val());
	cfg.setVariable("USER", tab.user.val());
	cfg.setVariable("NAME", tab.name.val());
	cfg.setVariable("PORT", (int)tab.port.val());
	cfg.setVariable("CHARSET", tab.charset.val());
	cfg.setVariable("PASSWORD", tab.passwd.val());

	DBConnectPool* pool = DBConnectPool::Create(cfg);

	if (pool == NULL)
	{
		LogTrace(eERR, "create database pool[%s] failed", id);

		return NULL;
	}

	return dbmap[id] = pool;
}

static int CheckAccess(const char* path, const char* param, const char* grouplist)
{
	if (path == NULL) return XG_ERROR;

	int res = RouteConfig::Instance()->checkAccess(path, param ? param : "", grouplist ? grouplist : "");

	if (res == XG_FAIL && app->getCgiAccess(path) == CGI_PUBLIC) return XG_AUTHFAIL;

	return res;
}

static int NotifyHost(const char* host, int port, const char* path, const char* param, const char* contype, const char* cookie)
{
	sp<NotifyItem> item = newsp<NotifyItem>();

	item->port = port;
	item->host = host;
	item->path = path;

	if (param) item->param = param;
	if (cookie) item->cookie = cookie;
	if (contype) item->contype = contype;

	return stdx::async(item) ? XG_OK : XG_SYSBUSY;
}

static int BroadcastHost(const char* path, const char* param, const char* contype, const char* cookie)
{
	int num = 0;
	set<string> hostset;
	vector<HostItem> vec = RouteConfig::Instance()->getList(path);

	for (const HostItem& item : vec)
	{
		if (hostset.insert(item.toString()).second)
		{
			int res = NotifyHost(item.host.c_str(), item.port, path, param, contype, cookie);

			if (res < 0) return res;

			++num;
		}
	}

	return num;
}

static int GetRegCenterHost(char* host, int* port)
{
	HostItem item = RouteConfig::Instance()->getHost();

	if (item.host.empty()) return XG_NOTFOUND;

	strcpy(host, item.host.c_str());
	*port = item.port;

	return XG_OK;
}

static int UpdateRouteList(const char* host, int port)
{
	return RouteConfig::Instance()->updateRoute(host, port) ? XG_OK : XG_SYSERR;
}

static int GetRouteHost(const char* path, char* host, int* port)
{
	HostItem item = RouteConfig::Instance()->get(path);

	if (item.host.empty()) return XG_NOTFOUND;

	strcpy(host, item.host.c_str());
	*port = item.port;

	return XG_OK;
}

static int GetLastRemoteStatus()
{
	return remotestatus;
}

static SmartBuffer GetRemoteResult(const char* path, const char* param, const char* contype, const char* cookie)
{
	int port;
	char host[64];
	HttpRequest request(path);
	sp<HttpResponse> response;

	if (param) request.setDataString(param);
	if (cookie && *cookie) request.setCookie(cookie);
	if (contype && *contype) request.setContentType(contype);

	if (GetRouteHost(request.getPath().c_str(), host, &port) < 0)
	{
		response = app->getLocaleResult(request);
	}
	else
	{
		sp<Socket> sock = SocketPool::Connect(host, port);

		if (!sock)
		{
			LogTrace(eERR, "request remote[%s] failed", path);

			remotestatus = XG_SENDFAIL;

			return SmartBuffer();
		}

		response = request.getResponse(sock);
	}

	if (!response)
	{
		LogTrace(eERR, "request remote[%s] failed", path);

		remotestatus = XG_RECVFAIL;

		return SmartBuffer();
	}

	remotestatus = response->getErrorCode();

	return response->getResult();
}

HTTP_PLUGIN_INIT({
	Process::SetObject("HTTP_GET_PARAMETER", (void*)GetParameter);
	Process::SetObject("HTTP_GET_DBCONNECT_POOL", (void*)GetDBConnectPool);
	Process::SetObject("HTTP_ASYNC_INIT_NEW_USER", (void*)AsyncInitNewUser);


	Process::SetObject("HTTP_NOTIFY_HOST_FUNC", (void*)NotifyHost);
	Process::SetObject("HTTP_CHECK_ACCESS_FUNC", (void*)CheckAccess);
	Process::SetObject("HTTP_GET_ROUTE_HOST_FUNC", (void*)GetRouteHost);
	Process::SetObject("HTTP_BROADCAST_HOST_FUNC", (void*)BroadcastHost);
	Process::SetObject("HTTP_GET_REMOTE_RESULT_FUNC", (void*)GetRemoteResult);
	Process::SetObject("HTTP_UPDATE_ROUTE_LIST_FUNC", (void*)UpdateRouteList);
	Process::SetObject("HTTP_GET_REG_CENTER_HOST_FUNC", (void*)GetRegCenterHost);
	Process::SetObject("HTTP_GET_LAST_REMOTE_STATUS_FUNC", (void*)GetLastRemoteStatus);
	Process::SetObject("HTTP_NEED_UPDATE_ROUTE_HOST_FUNC", (void*)NeedUpdateRouteHost);

	PingWorkThread::Instance()->start();

	RouteConfig::Instance()->init();

	return XG_OK;
})