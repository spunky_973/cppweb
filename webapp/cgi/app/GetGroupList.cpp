#include <webx/route.h>

class GetGroupList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetGroupList)

int GetGroupList::process()
{
	checkLogin();

	string sqlcmd;
	sp<DBConnect> dbconn = webx::GetDBConnect();

	stdx::format(sqlcmd, "SELECT * FROM T_XG_GROUP ORDER BY ID ASC");

	json["code"] = webx::PackJson(dbconn->query(sqlcmd), "list", json);
	out << json;

	return XG_OK;
}