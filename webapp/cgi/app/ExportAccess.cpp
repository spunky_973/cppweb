#include <webx/route.h>
#include <dbentity/T_XG_GROUP.h>

class ExportAccess : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(ExportAccess)

int ExportAccess::process()
{
	param_string(version);

	int res = 0;
	CT_XG_GROUP tab;
	sp<Session> session;

	if (session = webx::GetLocaleSession("ACCESS"))
	{
		if (version == session->get("version"))
		{
			out.printf("{\"code\":0,\"version\":\"%s\"}", version.c_str());
		}
		else
		{
			out << session->get("content");
		}

		return XG_OK;
	}

	typedef webx::AccessItem AccessItem;
	map<string, vector<AccessItem>> accessmap;

	if (!webx::LoadAccessMap(accessmap)) stdx::Throw(XG_SYSERR);
	
	JsonElement arr = json.addArray("list");

	for (auto& item : accessmap)
	{
		for (auto& tmp : item.second)
		{
			JsonElement data = arr[res++];

			data["path"] = item.first;
			data["param"] = tmp.getParamString();
			data["grouplist"] = tmp.getGroupString();
		}
	}

	string msg = json.toString();

	version = MD5GetEncodeString(msg.c_str(), msg.length(), true).val;
	session = webx::GetLocaleSession("ACCESS", res > 0 ? 24 * 3600 : 5);

	json["code"] = res;
	json["version"] = version;

	if (session)
	{
		session->set("version", version);
		session->set("content", json.toString());
	}

	out << json;

	return XG_OK;
}