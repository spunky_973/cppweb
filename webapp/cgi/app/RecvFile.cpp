#include <webx/route.h>

class RecvFile : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(RecvFile)

int RecvFile::process()
{
	SmartBuffer buffer;
	vector<webx::FileParamItem> vec;
	int res = webx::GetFileParamList(vec, buffer, app, request, response);

	if (res == XG_NETERR || res == XG_TIMEOUT) return XG_NETERR;

	if (res < 0) return simpleResponse(res);

	res = 0;

	JsonElement data = json["url"];

	for (auto& item : vec)
	{
		data[res++] = item.filepath.substr(app->getPath().length() - 1);
	}

	json["error"] = (int)(0);
	json["code"] = res;
	out << json;
	
	return XG_OK;
}