#include <webx/route.h>
#include <webx/route.h>
#include <dbentity/T_XG_CGI.h>

class GetCgiList : public webx::ProcessBase
{
protected:
	int process();
};

HTTP_WEBAPP(GetCgiList)

int GetCgiList::process()
{
	param_int(port);
	param_string(host);

	param_int(system);
	param_int(access);
	param_string(path);
	param_string(enabled);

	param_int(pagenum);
	param_int(pagesize);

	if (host.length() > 0)
	{
		webx::CheckSystemRight(this);

		HttpRequest request(this->request->getPath());

		request.setParameter("path", path);
		request.setParameter("system", system);
		request.setParameter("access", access);
		request.setParameter("enabled", enabled);
		request.setParameter("pagenum", pagenum);
		request.setParameter("pagesize", pagesize);

		SmartBuffer data = request.getResult(host, port);

		if (data.isNull()) return simpleResponse(XG_ERROR);

		out << data.str();

		return XG_OK;
	}

	if (pagesize <= 0)
	{
		pagesize = 10000000;
		pagenum = 0;
	}

	if (!RedisConnect::CanUse())
	{
		param_int(redisport);
		param_string(redishost);
		param_string(redispasswd);

		if (redisport > 0 && redishost.length() > 0)
		{
			Socket sock;

			if (sock.connect(redishost, redisport))
			{
				RedisConnect::Setup(redishost, redisport, redispasswd);

				sp<RedisConnect> redis = RedisConnect::Instance();

				if (redis)
				{
					LogTrace(eIMP, "initialize redis[%s:%d] success", redishost.c_str(), redisport);
				}
				else
				{
					LogTrace(eERR, "initialize redis[%s:%d] failed", redishost.c_str(), redisport);
				}
			}
		}
	}

	if (!webx::GetLogHost().canUse())
	{
		param_int(logport);
		param_string(loghost);

		if (logport > 0)
		{
			if (loghost.empty() || loghost == LOCAL_IP)
			{
				loghost = response->getSocket()->getAddress().host;

				if (loghost.empty()) return simpleResponse(XG_SYSERR);
			}

			if (webx::SetLogHost(loghost, logport) > 0)
			{
				LogTrace(eINF, "update log host[%s:%d] success", loghost.c_str(), logport);
			}
			else
			{
				LogTrace(eERR, "update log host[%s:%d] failed", loghost.c_str(), logport);
			}
		}
	}

	if (webx::NeedUpdateRouteHost())
	{
		param_int(routeport);
		param_string(routehost);

		if (routeport > 0)
		{
			if (routehost.empty() || routehost == LOCAL_IP)
			{
				routehost = response->getSocket()->getAddress().host;

				if (routehost.empty()) return simpleResponse(XG_SYSERR);
			}

			if (webx::UpdateRouteList(routehost, routeport) > 0)
			{
				LogTrace(eINF, "update route host[%s:%d] success", routehost.c_str(), routeport);
			}
			else
			{
				LogTrace(eERR, "update route host[%s:%d] failed", routehost.c_str(), routeport);
			}
		}
	}

	int res = 0;
	CT_XG_CGI tab;
	CgiMapData* cgi = NULL;
	map<string, CgiMapData> cgimap;

	tab.init(webx::GetDBConnect());

	if (app->getCgiMap(cgimap) > 0 && tab.find("1=1"))
	{
		int len = 0;
		int type = -1;
		vector<string> vec;

		if (enabled.length() > 0) type = stdx::atoi(enabled.c_str());

		stdx::tolower(path);

		for (auto& item : cgimap)
		{
			item.second.code = 1;
			item.second.param.clear();
		}

		while (tab.next())
		{
			auto it = cgimap.find(tab.path.val());

			if (it == cgimap.end())
			{
				cgi = &cgimap[tab.path.val()];
				cgi->flag = CgiMapData::EXE_FLAG;
				cgi->code = 0;
			}
			else
			{
				cgi = &it->second;
				cgi->code = 2;
			}

			cgi->maxsz = tab.maxsz.val();
			cgi->param = tab.remark.val();
			cgi->maxcnt = tab.maxcnt.val();
			cgi->access = tab.enabled.val();
			cgi->hostmaxcnt = tab.hostmaxcnt.val();
		}

		auto docmap = app->getCgiDocMap();
		auto datmap = app->getTransCountMap();
		JsonElement arr = json.addArray("list");
		const string& dllpath = request->getCgiData().url;

		for (auto& item : cgimap)
		{
			if (item.second.flag == CgiMapData::URL_FLAG) continue;

			if (system < 1 && item.second.access < CGI_PROTECT)
			{
				const string& path = item.second.url;

				if (path == dllpath) continue;

				if (strstr(path.c_str(), "/etc/plugin/bin/")) continue;
				if (strstr(path.c_str(), "/app/compile/cgi/")) continue;
				if (strstr(path.c_str(), "/app/confile/cgi/")) continue;
				if (strstr(path.c_str(), "/app/product/cgi/")) continue;
				if (strstr(path.c_str(), "/app/promote/cgi/")) continue;
				if (strstr(path.c_str(), "/app/workspace/cgi/")) continue;
			}

			if (path.length() > 0 && item.first.find(path) == string::npos) continue;
			
			if (access > 0)
			{
				if (access <= item.second.access) vec.push_back(item.first);
			}
			else
			{
				if (type < 0 || type == item.second.access) vec.push_back(item.first);
			}
		}

		len = vec.size();

		std::sort(vec.begin(), vec.end());

		for (int i = 0; i < len; i++)
		{
			const auto& key = vec[i];
			const auto& item = cgimap[key];

			if (res < pagesize && i >= pagenum * pagesize)
			{
				auto it = datmap.find(key);
				JsonElement data = arr[res++];

				if (item.param.empty())
				{
					auto it = docmap.find(key);

					data["remark"] = it == docmap.end() ? item.param : get<2>(it->second);
				}
				else
				{
					data["remark"] = item.param;
				}

				data["path"] = key;
				data["flag"] = item.code;
				data["maxsz"] = item.maxsz;
				data["maxcnt"] = item.maxcnt;
				data["enabled"] = item.access;
				data["extdata"] = item.extdata;
				data["hostmaxcnt"] = item.hostmaxcnt;

				if (it == datmap.end())
				{
					data["meancost"] = (int)(0);
					data["daily"] = (int)(0);
				}
				else
				{
					data["meancost"] = it->second.meancost;
					data["daily"] = it->second.daily;
				}
			}
		}

		json["count"] = vec.size();
	}

	try
	{
		checkLogin();
		checkSystemRight();

		json["system"] = (int)(1);
	}
	catch(Exception e)
	{
		json["system"] = (int)(0);
	}

	json["code"] = res;
	out << json;

	return XG_OK;
}