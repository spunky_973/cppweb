#ifndef XG_JSON_H
#define XG_JSON_H
////////////////////////////////////////////////////////////////////
extern "C"
{
	#include "src/cJSON.h"
}

#include "../stdx/Reflect.h"

#define REGEX_NAME "[a-zA-Z_][a-zA-Z0-9_]{0,63}"
#define REGEX_MAIL "([0-9A-Za-z\\-_\\.]+)@([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)"
#define REGEX_HOST "([0-9A-Za-z\\-_\\.]+)\\.([0-9a-z]+\\.[a-z]{2,3}(\\.[a-z]{2})?)"
#define REGEX_IPV4 "((25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])\\.){3}(25[0-5]|2[0-4][0-9]|1[0-9][0-9]|[1-9][0-9]|[0-9])"
#define REGEX_MOBILE "1[2-9][0-9]{9}"

#define REGEX_DIGIT(minlen, maxlen) "[0-9]{"#minlen","#maxlen"}"
#define REGEX_ALPHA(minlen, maxlen) "[a-z|A-Z]{"#minlen","#maxlen"}"
#define REGEX_ALNUM(minlen, maxlen) "[0-9|a-z|A-Z]{"#minlen","#maxlen"}"

class JsonElement : public Object
{
public:
	class iterator
	{
		friend class JsonElement;

	private:
		int idx;
		int len;
		cJSON* elem;
		sp<cJSON> holder;

		iterator(cJSON* item, JsonElement* parent);

	public:
		iterator& operator ++ ();
		JsonElement operator * ();
		bool operator != (const iterator& obj) const;
	};

protected:
	cJSON* elem;
	sp<cJSON> holder;

	static bool InitElement(cJSON* item);
	static bool InitElement(cJSON* item, int val);
	static bool InitElement(cJSON* item, bool val);
	static bool InitElement(cJSON* item, long val);
	static bool InitElement(cJSON* item, double val);
	static bool InitElement(cJSON* item, const char* val);

public:
	int size() const;
	JsonElement add();
	void remove(int idx);
	string toString() const;
	bool load(const string& msg);
	JsonElement get(int idx) const;
	void remove(const string& name);
	bool init(const string& msg = "");
	JsonElement add(const string& name);
	JsonElement addArray(const string& name);
	JsonElement addObject(const string& name);
	JsonElement get(const string& name) const;

	JsonElement()
	{
		elem = NULL;
	}
	JsonElement(const string& msg)
	{
		elem = NULL;
		init(msg);
	}
	JsonElement(cJSON* item, const JsonElement* parent)
	{
		holder = parent ? parent->holder : NULL;
		elem = item;
	}

public:
	iterator end()
	{
		return iterator(NULL, this);
	}
	iterator begin()
	{
		return iterator(elem, this);
	}
	string getName() const
	{
		return elem ? elem->string : stdx::EmptyString();
	}
	JsonElement pack(cJSON* item) const
	{
		return JsonElement(item, this);
	}

public:
	bool isNull() const
	{
		return elem == NULL || elem->type == cJSON_NULL;
	}
	bool isBool() const
	{
		return elem && (elem->type == cJSON_True || elem->type == cJSON_False);
	}
	bool isArray() const
	{
		return elem && elem->type == cJSON_Array;
	}
	bool isObject() const
	{
		return elem && elem->type == cJSON_Object;
	}
	bool isNumber() const
	{
		return elem && elem->type == cJSON_Number;
	}
	bool isString() const
	{
		return elem && elem->type == cJSON_String;
	}
	bool isNull(int idx) const
	{
		return get(idx).isNull();
	}
	bool isBool(int idx) const
	{
		return get(idx).isBool();
	}
	bool isArray(int idx) const
	{
		return get(idx).isArray();
	}
	bool isObject(int idx) const
	{
		return get(idx).isObject();
	}
	bool isNumber(int idx) const
	{
		return get(idx).isNumber();
	}
	bool isString(int idx) const
	{
		return get(idx).isString();
	}
	bool isNull(const string& name) const
	{
		return get(name).isNull();
	}
	bool isBool(const string& name) const
	{
		return get(name).isBool();
	}
	bool isArray(const string& name) const
	{
		return get(name).isArray();
	}
	bool isObject(const string& name) const
	{
		return get(name).isObject();
	}
	bool isNumber(const string& name) const
	{
		return get(name).isNumber();
	}
	bool isString(const string& name) const
	{
		return get(name).isString();
	}

public:
	bool setVariable(int val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(bool val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(long val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(double val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(const char* val)
	{
		return InitElement(elem, val);
	}
	bool setVariable(const string& val)
	{
		return InitElement(elem, val.c_str());
	}

	string getVariable() const
	{
		if (elem == NULL) return stdx::EmptyString();
		if (elem->type == cJSON_True) return "true";
		if (elem->type == cJSON_False) return "false";
		if (elem->type == cJSON_Number) return stdx::str(elem->valuedouble);
		return elem->valuestring ? elem->valuestring : stdx::EmptyString();
	}
	bool getVariable(bool& val) const
	{
		CHECK_FALSE_RETURN(elem && (elem->type == cJSON_True || elem->type == cJSON_False));
		val = elem->type == cJSON_True;
		return true;
	}
	bool getVariable(int& val) const
	{
		CHECK_FALSE_RETURN(elem && elem->type == cJSON_Number);
		val = elem->valueint;
		return true;
	}
	bool getVariable(long& val) const
	{
		CHECK_FALSE_RETURN(elem && elem->type == cJSON_Number);
		val = elem->valueint;
		return true;
	}
	bool getVariable(double& val) const
	{
		CHECK_FALSE_RETURN(elem && elem->type == cJSON_Number);
		val = elem->valuedouble;
		return true;
	}
	bool getVariable(string& val) const
	{
		CHECK_FALSE_RETURN(elem && elem->valuestring);
		val = elem->valuestring;
		return true;
	}

	int asInt() const
	{
		int val = 0;
		if (getVariable(val)) return val;
		return stdx::atoi(getVariable().c_str());
	}
	long asLong() const
	{
		long val = 0;
		if (getVariable(val)) return val;
		return stdx::atol(getVariable().c_str());
	}
	bool asBool() const
	{
		string str = getVariable();
		if (str.empty()) return false;
		if (stdx::atoi(str.c_str())) return true;
		return strcasecmp(str.c_str(), "true") == 0;
	}
	double asDouble() const
	{
		double val = 0;
		if (getVariable(val)) return val;
		return stdx::atof(getVariable().c_str());
	}
	string asString() const
	{
		return getVariable();
	}
	int asInt(int idx) const
	{
		return get(idx).asInt();
	}
	long asLong(int idx) const
	{
		return get(idx).asLong();
	}
	bool asBool(int idx) const
	{
		return get(idx).asBool();
	}
	double asDouble(int idx) const
	{
		return get(idx).asDouble();
	}
	string asString(int idx) const
	{
		return get(idx).asString();
	}
	int asLong(const string& name) const
	{
		return get(name).asLong();
	}
	int asInt(const string& name) const
	{
		return get(name).asInt();
	}
	bool asBool(const string& name) const
	{
		return get(name).asBool();
	}
	double asDouble(const string& name) const
	{
		return get(name).asDouble();
	}
	string asString(const string& name) const
	{
		return get(name).asString();
	}

public:
	JsonElement operator [] (int idx);
	JsonElement operator [] (const string& name);

	JsonElement& operator = (int val)
	{
		setVariable(val);
		return *this;
	}
	JsonElement& operator = (bool val)
	{
		setVariable(val);
		return *this;
	}
	JsonElement& operator = (long val)
	{
		setVariable(val);
		return *this;
	}
	JsonElement& operator = (size_t val)
	{
		setVariable((long)(val));
		return *this;
	}
	JsonElement& operator = (double val)
	{
		setVariable(val);
		return *this;
	}
	JsonElement& operator = (const char* val)
	{
		setVariable(val);
		return *this;
	}
	JsonElement& operator = (const string& val)
	{
		setVariable(val);
		return *this;
	}
	JsonElement& operator = (const JsonElement& val)
	{
		holder = val.holder;
		elem = val.elem;
		return *this;
	}
};

static string& operator += (string& str, const JsonElement& obj)
{
	return str += obj.toString();
}

static ostream& operator << (ostream& out, const JsonElement& obj)
{
	return out << obj.toString();
}

static StringCreator& operator << (StringCreator& out, const JsonElement& obj)
{
	return out << obj.toString();
}

static string operator + (const string& str, const JsonElement& obj)
{
	return str + obj.toString();
}

class JsonReflect : public Object
{
public:
	virtual string toString() const;
	virtual string toDocString() const;
	virtual bool fromString(const string& msg);
	virtual bool fromObject(const JsonReflect& obj);
};

template <class DATA_TYPE>
class JsonReflectList : public JsonReflect
{
protected:
	robject(vector<sp<DATA_TYPE>>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	size_t size() const
	{
		return vec.size();
	}
	sp<DATA_TYPE> add()
	{
		sp<DATA_TYPE> item = newsp<DATA_TYPE>();

		vec.push_back(item);

		return item;
	}
	void add(sp<DATA_TYPE> val)
	{
		vec.push_back(val);
	}
	void add(const DATA_TYPE& val)
	{
		vec.push_back(newsp<DATA_TYPE>(val));
	}
	sp<DATA_TYPE> get(int idx) const
	{
		return vec[idx];
	}
	const vector<sp<DATA_TYPE>>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++].load(item->toString());

		return res.toString();
	}
	string toDocString() const
	{
		return DATA_TYPE().toDocString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src)
		{
			sp<DATA_TYPE> dest = newsp<DATA_TYPE>();

			if (item.isString() || item.isArray() || item.isObject())
			{
				CHECK_FALSE_RETURN(dest->fromString(item.toString()));
			}
			else
			{
				CHECK_FALSE_RETURN(dest->fromString(item.getVariable()));
			}

			vec.push_back(dest);
		}

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<int> : public JsonReflect
{
protected:
	robject(vector<int>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	void add(int val)
	{
		vec.push_back(val);
	}
	size_t size() const
	{
		return vec.size();
	}
	int get(int idx) const
	{
		return vec[idx];
	}
	const vector<int>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++] = item;

		return res.toString();
	}
	string toDocString() const
	{
		return stdx::EmptyString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.asInt());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<bool> : public JsonReflect
{
protected:
	robject(vector<bool>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	void add(bool val)
	{
		vec.push_back(val);
	}
	size_t size() const
	{
		return vec.size();
	}
	bool get(int idx) const
	{
		return vec[idx];
	}
	const vector<bool>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++] = item;

		return res.toString();
	}
	string toDocString() const
	{
		return stdx::EmptyString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.asBool());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<float> : public JsonReflect
{
protected:
	robject(vector<float>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	size_t size() const
	{
		return vec.size();
	}
	void add(float val)
	{
		vec.push_back(val);
	}
	float get(int idx) const
	{
		return vec[idx];
	}
	const vector<float>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++] = item;

		return res.toString();
	}
	string toDocString() const
	{
		return stdx::EmptyString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.asDouble());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<double> : public JsonReflect
{
protected:
	robject(vector<double>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	size_t size() const
	{
		return vec.size();
	}
	void add(double val)
	{
		vec.push_back(val);
	}
	double get(int idx) const
	{
		return vec[idx];
	}
	const vector<double>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto item : vec) res[idx++] = item;

		return res.toString();
	}
	string toDocString() const
	{
		return stdx::EmptyString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.asDouble());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

template <>
class JsonReflectList<string> : public JsonReflect
{
protected:
	robject(vector<string>, vec);

public:
	void clear()
	{
		vec.clear();
	}
	bool empty() const
	{
		return vec.empty();
	}
	size_t size() const
	{
		return vec.size();
	}
	void add(const string& val)
	{
		vec.push_back(val);
	}
	string get(int idx) const
	{
		return vec[idx];
	}
	const vector<string>& getList() const
	{
		return vec;
	}

public:
	string toString() const
	{
		int idx = 0;
		JsonElement res;

		for (auto& item : vec) res[idx++] = item;

		return res.toString();
	}
	string toDocString() const
	{
		return stdx::EmptyString();
	}
	bool fromString(const string& msg)
	{
		JsonElement src(msg);

		if (src.isNull()) return true;

		CHECK_FALSE_RETURN(src.isArray());

		for (auto item : src) vec.push_back(item.getVariable());

		return true;
	}
	bool fromObject(const JsonReflect& obj)
	{
		return fromString(obj.toString());
	}
};

#define JsonEntity(clazz) struct clazz : public JsonReflect

#define rarray(type, ...) robject(JsonReflectList<type>, __VA_ARGS__)
#define narray(type, ...) robject(JsonReflectList<type>, __VA_ARGS__, "optional")

////////////////////////////////////////////////////////////////////
#endif