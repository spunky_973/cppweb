#include <stdx/all.h>

#ifdef XG_WITHJAVA

#include <java/JavaLoader.h>

#define cppstr(str) JavaLoader::CppString(env, str)
#define javastr(str) JavaLoader::JavaString(env, str)

EXTERN_DLL_FUNC jstring JNICALL Java_stdx_Utils_GetEnv(JNIEnv* env, jclass obj, jstring key)
{
	return javastr(proc::env(cppstr(key)));
}

EXTERN_DLL_FUNC jstring JNICALL Java_stdx_Utils_SetEnv(JNIEnv* env, jclass obj, jstring key, jstring val)
{
	return javastr(proc::env(cppstr(key), cppstr(val)));
}

#endif