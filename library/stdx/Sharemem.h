#ifndef XG_SHAREMEM_H
#define XG_SHAREMEM_H
////////////////////////////////////////////////////////
#include "std.h"

class Sharemem : public Object
{
	class Data : public Object
	{
	public:
		u_char* data;

		Data();
		~Data();
		void close();
		u_char* get() const;
		bool init(HANDLE handle);
	};

protected:
	bool created;
	HANDLE handle;
	sp<Data> data;

public:
	Sharemem();
	~Sharemem();
	void close();
	int size() const;
	bool canUse() const;
	u_char* get() const;
	u_char* open(HANDLE handle);
	u_char* open(const string& name);
	u_char* create(const string& name, int memsz = 4096);
};
////////////////////////////////////////////////////////
#endif